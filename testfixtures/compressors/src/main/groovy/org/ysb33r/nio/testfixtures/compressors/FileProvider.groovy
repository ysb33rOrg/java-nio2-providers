/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.testfixtures.compressors

import groovy.transform.CompileStatic
import org.apache.commons.io.FileUtils

import java.nio.file.Files
import java.nio.file.OpenOption
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardOpenOption

import static java.nio.file.StandardOpenOption.CREATE_NEW
import static java.nio.file.StandardOpenOption.WRITE

@CompileStatic
class FileProvider {
    final String scheme
    final String ext
    final File workFolder

    static final File RO_FOLDER = findReadOnlyFolder('testfixtures', 'compressors/src/main/resources/test-files')

    FileProvider(final String scheme, final String ext, final File workFolder) {
        this.scheme = scheme
        this.ext = ext
        this.workFolder = workFolder
    }

    String expectedContents(final String name = 'file.txt') {
        '''contents
'''
    }

    File roCompressedFile(final String name = 'file.txt') {
        new File(RO_FOLDER, "${name}.${ext}")
    }

    Path roCompressedPath(final String name = 'file.txt') {
        Paths.get("${scheme}:${roCompressedFile(name).toURI()}".toURI())
    }

    File rwCompressedFile(final String name = 'file.txt') {
        File ioFile = new File(workFolder, "${name}.${ext}")
        ioFile.parentFile.mkdirs()
        ioFile
    }

    Path rwCompressedPath(final String name = 'file.txt') {
        File ioFile = rwCompressedFile(name)
        Paths.get("${scheme}:${ioFile.toURI()}".toURI())
    }

    File rwDecompressedFile(final String name = 'file.txt') {
        File ioFile = new File(workFolder, name)
        ioFile.parentFile.mkdirs()
        ioFile
    }

    Path rwDecompressedPath(final String name = 'file.txt') {
        rwDecompressedFile(name).toPath()
    }

    File createCompressedFile(final String name = 'file.txt') {
        File compressedFile = new File(workFolder, "${name}.${ext}")
        FileUtils.copyFile(
            new File(RO_FOLDER, "${name}.${ext}"),
            compressedFile
        )
        compressedFile
    }

    Path createCompressedPath(final String name = 'file.txt') {
        Paths.get("${scheme}:${createCompressedFile(name).toURI()}".toURI())
    }

    String readContentsFromCreatedCompressedPath(final String name = 'file.txt') {
        readContentsFromCreatedCompressedPath(rwCompressedPath(name))
    }

    String readContentsFromCreatedCompressedPath(Path path) {
        InputStream readBack = Files.newInputStream(path, StandardOpenOption.READ)
        String data = readBack.text
        readBack.close()
        data
    }

    String readContentsFromCreatedCompressedPath(OpenOption withOption, final String name = 'file.txt') {
        readContentsFromCreatedCompressedPath(withOption, rwCompressedPath(name))
    }

    String readContentsFromCreatedCompressedPath(OpenOption withOption, Path path) {
        InputStream readBack = Files.newInputStream(path, withOption, StandardOpenOption.READ)
        String data = readBack.text
        readBack.close()
        data
    }

    File symlinkFile(File path) {
        final String linkName = "linked-${path.name}"
        final File linkFile = new File(path.parentFile, linkName)
        Files.createSymbolicLink(linkFile.toPath(), path.toPath()).toFile()
    }

    String writeDataToFileWithOptionsAndReadback(final Path testFile, OpenOption testWithOption, final String contents) {
        OutputStream strm = Files.newOutputStream(
            testFile,
            testWithOption, WRITE, CREATE_NEW
        )
        strm << contents
        strm.close()
        InputStream readBack = Files.newInputStream(testFile, StandardOpenOption.READ)
        String data = readBack.text
        readBack.close()
        data

    }
    private static File findReadOnlyFolder(final String toplevelFolder, final String relativePath) {
        File current = new File('.').absoluteFile
        File startedAt = current

        while (current != null && current.name.size()) {
            File[] searchResult = current.listFiles(new FileFilter() {
                @Override
                boolean accept(File pathname) {
                    pathname.name == toplevelFolder && pathname.isDirectory()
                }
            })

            if (searchResult.size() == 1) {
                File result = new File(searchResult[0], relativePath)
                if (result.exists()) {
                    return result.absoluteFile
                }
            } else if (searchResult.size() > 1) {
                throw new UnsupportedOperationException("Cannot have more than one file named '${toplevelFolder}'")
            }

            current = current.parentFile
        }

        throw new FileNotFoundException("Cannot find a folder called '${toplevelFolder}' which contains '${relativePath}'. Started at ${startedAt}.")
    }
}
