/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.gz

import spock.lang.Specification
import spock.lang.Unroll

import java.nio.file.Path
import java.nio.file.Paths

class GzFilePathSpec extends GzSpecification {

    @Unroll
    def 'Treat #uri as a valid URI'() {

        when:
        GzFilePath gzPath = (GzFilePath)(Paths.get(uri.toURI()))
        Path servicePath = gzPath.getServiceLayerPath()

        then:
        gzPath.absolute == absolute
        servicePath.isAbsolute() == absolute
        servicePath.fileSystem.provider().scheme == 'file'

        cleanup:
        gzPath.fileSystem.close()

        where:
        uri                  || absolute
        'gz:file.gz'         || false
        'gz:/file.gz'        || true
        'gz:file:///file.gz' || true


    }
}