/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.gz

import spock.lang.Stepwise

import java.nio.channels.ByteChannel
import java.nio.file.FileSystem
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.StandardOpenOption

@Stepwise
class GzFileSystemSpec extends GzSpecification {

    void 'Closing a filesystem'() {
        when: 'A filesystem and associated data channel are created'
        File ioFile = roCompressedFile()
        Path testFile = roCompressedPath()
        FileSystem fs = testFile.fileSystem
        ByteChannel channel = Files.newByteChannel(testFile, StandardOpenOption.READ)

        then: 'The filesystem status is open'
        fs.isOpen()

        and: 'The associated channel is open'
        channel.isOpen()

        when: 'The filesystem is closed'
        fs.close()

        then: 'The filesystem will report as closed'
        !fs.isOpen()

        and: 'The channel is closed'
        !channel.isOpen()

        cleanup:
        testFile?.fileSystem?.close()

    }

    void 'Open two paths on same secondary filesystem'() {
        when:
        Path testFile = rwCompressedPath()
        Path testFile2 = rwCompressedPath('file2.txt')

        then:
        noExceptionThrown()

        cleanup:
        testFile?.fileSystem?.close()
        testFile2?.fileSystem?.close()

    }

    void 'Root filesystems are empty'() {
        expect:
        roCompressedPath().fileSystem.rootDirectories.size() == 0
    }

    void 'Supported file attribute views'() {
        when:
        Set<String> views = roCompressedPath().fileSystem.supportedFileAttributeViews()

        then:
        views.size() == 1
        views.contains('basic')
    }

    void 'UserPrincipleLookupService is not supported'() {
        when:
        roCompressedPath().fileSystem.userPrincipalLookupService

        then:
        thrown(UnsupportedOperationException)
    }

    void 'newWatchService is not supported'() {
        when:
        roCompressedPath().fileSystem.newWatchService()

        then:
        thrown(UnsupportedOperationException)
    }

}