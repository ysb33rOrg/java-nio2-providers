/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.gz

import spock.lang.Stepwise
import spock.lang.Unroll

import java.nio.file.*

import static java.nio.file.StandardOpenOption.APPEND
import static java.nio.file.StandardOpenOption.CREATE_NEW
import static java.nio.file.StandardOpenOption.READ
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING
import static java.nio.file.StandardOpenOption.WRITE

class GzFileInOutSpec extends GzSpecification {

    void 'Compressed file exists'() {
        given:
        File ioFile = roCompressedFile()
        Path testFile = roCompressedPath()

        expect:
        ioFile.exists()
        Files.exists(testFile)

        cleanup:
        testFile?.fileSystem?.close()
    }

    void 'Reading from a compressed file'() {
        given:
        File ioFile = roCompressedFile()
        Path testFile = roCompressedPath()

        when:
        InputStream strm = Files.newInputStream(testFile, READ)
        String data = strm.text
        strm.close()

        then:
        data.startsWith('content')

        cleanup:
        testFile?.fileSystem?.close()
    }

    @Unroll
    void 'Combining invalid combinations for reading (#options)'() {
        given:
        Path testFile = roCompressedPath()

        when:
        InputStream strm = Files.newInputStream(testFile, (OpenOption[]) options.toArray())

        then:
        thrown(exceptClass)

        cleanup:
        testFile?.fileSystem?.close()

        where:
        exceptClass                   | options
        UnsupportedOperationException | [READ, WRITE]
        IllegalArgumentException      | [READ, TRUNCATE_EXISTING]
        UnsupportedOperationException | [WRITE]
        UnsupportedOperationException | [APPEND]
    }

    void "Writing to a compressed file"() {
        given:
        Path testFile = rwCompressedPath()
        File ioFile = rwCompressedFile()

        when:
        OutputStream strm = Files.newOutputStream(
            testFile,
            WRITE, CREATE_NEW
        )
        strm << 'contents'
        strm.close()

        then:
        ioFile.exists()
        Files.exists(testFile)

        when:
        InputStream readBack = Files.newInputStream(testFile, READ)
        String data = readBack.text
        readBack.close()

        then:
        data == 'contents'

        cleanup:
        testFile?.fileSystem?.close()
    }

    @Unroll
    void 'Combining invalid combinations for writing (#options)'() {
        given:
        File ioFile = roCompressedFile()
        Path testFile = roCompressedPath()

        when:
        OutputStream strm = Files.newOutputStream(testFile, (OpenOption[]) options.toArray())

        then:
        thrown(exceptClass)

        cleanup:
        testFile?.fileSystem?.close()

        where:
        exceptClass                   | options
        IllegalArgumentException      | [READ, WRITE]
        UnsupportedOperationException | [APPEND]
    }

    void 'Copy from a compression filesystem'() {

        given: 'A file on a compressed filesystem'
        Path inputPath = createCompressedPath()
        File destFile = rwDecompressedFile()
        Path destPath = destFile.toPath()
        assert Files.notExists(destPath)

        when: 'The file is copied to a normal filesystem'
        Files.copy(inputPath, destPath)
        String data = readContentsFromCreatedCompressedPath(destPath)

        then: 'The contents will be decompressed into the destination'
        data == "contents\n"

        cleanup:
        inputPath?.fileSystem?.close()
    }

    void 'Copy into a compressed filesystem'() {
        given: 'A file on a filesystem'
        File inputFile = rwDecompressedFile('src.txt')
        Path inputPath = inputFile.toPath()
        Path destPath = rwCompressedPath()
        File destFile = rwCompressedFile()
        assert Files.notExists(destPath)

        when: 'The file is copied to a compressed filesystem'
        inputFile.text = 'foobar'
        Files.copy(inputPath, destPath)
        String data = readContentsFromCreatedCompressedPath(destPath)

        then: 'The contents will be compressed into the destination'
        destFile.text != 'foobar'
        Files.exists(destPath)
        data == 'foobar'

        when: 'New data is copied to a compressed filesystem'
        inputFile.text = 'barfoo'
        Files.copy(inputPath, destPath, StandardCopyOption.REPLACE_EXISTING)
        data = readContentsFromCreatedCompressedPath(destPath)

        then: 'The contents will be compressed into the destination and the original overwritten'
        destFile.text != 'barfoo'
        data == 'barfoo'

        cleanup:
        destPath?.fileSystem?.close()
    }

    void 'Copy from compressed filesystem to compressed filesystem'() {

        given:
        Path inputPath = roCompressedPath()
        Path destPath = rwCompressedPath()
        assert Files.notExists(destPath)

        when: 'Copying this file'
        inputPath.fileSystem.provider().copy(inputPath, destPath)
        String data = readContentsFromCreatedCompressedPath(destPath)

        then: 'The destination will contain the correct content'
        data == "contents\n"

        cleanup:
        destPath?.fileSystem?.close()
        inputPath?.fileSystem?.close()
    }

    void 'Move into compressed filesystem'() {
        given: 'A file on a filesystem'
        Path inputPath = rwDecompressedPath('src.txt')
        File inputFile = inputPath.toFile()
        Path destPath = rwCompressedPath()
        File destFile = rwCompressedFile()
        assert Files.notExists(destPath)

        when: 'The file is moved to a compressed filesystem'
        inputFile.text = 'foobar'
        Files.move(inputPath, destPath)
        String data = readContentsFromCreatedCompressedPath(destPath)

        then: 'The contents will be compressed into the destination'
        destFile.text != 'foobar'
        Files.exists(destPath)
        data == 'foobar'

        and: 'The original file will no longer exist'
        !inputFile.exists()

        when: 'A second file is moved to a compressed filesystem with the same destination name'
        inputFile.text = 'foobar2'
        Files.move(inputPath, destPath)

        then: 'It is not allowed'
        thrown(FileAlreadyExistsException)

        when: 'A move is accompanied by an explicit replace, the file will be moved'
        Files.move(inputPath, destPath, StandardCopyOption.REPLACE_EXISTING)
        data = readContentsFromCreatedCompressedPath(destPath)
        then:
        data == 'foobar2'

        cleanup:
        destPath?.fileSystem?.close()
    }

    void 'Move out of compressed filesystem'() {
        given: 'A file on a compressed filesystem'
        Path inputPath = createCompressedPath()
        Path destPath = rwDecompressedPath()
        assert Files.notExists(destPath)

        when: 'The file is moved out of the compressed filesystem'
        Files.move(inputPath, destPath)

        then: 'The decompressed file will will exist on the filesystem'
        Files.exists(destPath)
        destPath.toFile().text == expectedContents()

        and: 'The compressed file will no longer exist'
        !Files.exists(inputPath)

        when: 'A file is moved to the same destination again'
        inputPath = createCompressedPath()
        Files.move(inputPath, destPath)

        then: 'It is not allowed'
        thrown(FileAlreadyExistsException)
        Files.exists(inputPath)

        when: 'A move is accompanied by an explicit replace, the file will be moved'
        Files.move(inputPath, destPath, StandardCopyOption.REPLACE_EXISTING)

        then: 'It is allowed'
        Files.exists(destPath)

        and: 'The compressed file will no longer exist'
        !Files.exists(inputPath)

        cleanup:
        inputPath?.fileSystem?.close()
    }

    void 'Move from compressed filesystem to compressed filesystem'() {
        given: 'Two compressed filesystems where the second file does not exist'
        Path inputPath = createCompressedPath()
        Path destPath = rwCompressedPath('dest')

        when: 'The first compressed file is moved to the second'
        Files.move(inputPath, destPath)
        String data = readContentsFromCreatedCompressedPath(destPath)

        then: 'The second file is created with the correct content'
        data == expectedContents()

        and: 'The first file is deleted'
        !Files.exists(inputPath)
    }
}