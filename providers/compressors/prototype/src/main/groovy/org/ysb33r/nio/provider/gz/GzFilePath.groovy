/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.gz

import org.ysb33r.nio.provider.commons_core.AbstractCompressorFilePath
import java.nio.file.Path
import groovy.transform.CompileStatic


/** Represents a path on a @schemeName@@ compressed filesystem.
 *
 * @since 0.1
 */
@CompileStatic
class GzFilePath extends AbstractCompressorFilePath {

    /** Constructs a GZIP compressed path.
     *
     * GZIP paths are of the format
     *
     * <pre>
     *   gz:/path/to/file.gz
     *   gz:file:///path/to/file.gz
     *   gz:http://server/path/to/file.gz
     *
     * </pre>
     *
     * Compressed streams are always serviced from another filesystem.
     *
     * @param fs Filesystem this path is tied to.
     * @param fileServiceLayer Path that will actually service the file
     */
    GzFilePath(GzFileSystem fs, Path fileServiceLayer) {
        super(fs,fileServiceLayer,org.ysb33r.nio.provider.gz.GzFilePath)
    }


}
