/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.gz

import groovy.transform.CompileStatic
import org.ysb33r.nio.provider.commons_core.AbstractCompressorFileSystemProvider

/** Provides GZIP-compressed filesystems with 'gz' as the default scheme.
 *
 * The default scheme can be overridden by setting system property {@code org.ysb33r.nio.provider.gz.scheme}
 * to something else before this object is instantiated.
 * 
 * @since 0.1
 */
@CompileStatic
class GzFileSystemProvider extends AbstractCompressorFileSystemProvider {

    GzFileSystemProvider() {
        super('gz', GzFileSystem, GzFilePath)
    }

    /** The set of attribute view classes directly supported by the 'gz' compressed file system.
     *
     * @return Set of attribute views. Can be empty, but never {@code null}.
     *
     * @since 0.1
     */
    @Override
    protected Set<Class> getSupportedAttributeViews() {
        (Set) []
    }

    /** Returns the supported views in a map keyed by view name.
     *
     * @return Map of view name + implementing class name
     *
     * @since 0.1
     */
    @Override
    protected Map<String, Class> getSupportedAttributeViewNames() {
        (Map<String, Class>) []
    }
}

