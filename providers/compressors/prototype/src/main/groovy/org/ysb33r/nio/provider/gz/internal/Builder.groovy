/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.gz.internal

import groovy.transform.CompileStatic
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream
import org.apache.commons.compress.compressors.gzip.GzipParameters
import org.ysb33r.nio.provider.core.ProviderOpts

import java.nio.file.OpenOption

/** Implements specific options for GZIP.
 *
 * @since 0.1
 */
@CompileStatic
class Builder {

    /** A list of short option names that are valid of GZIP.
     *
     * @return List of valid short option names
     */
    static Iterable<String> getOptionNames() {
        OPTION_NAMES
    }

    static OutputStream buildOutputStream(OutputStream secondaryStream, Iterable<OpenOption> compressionOptions) {
        new GzipCompressorOutputStream(secondaryStream, Builder.getParametersFromOptions(compressionOptions))
    }

    static InputStream buildInputStream(InputStream wrapped, Iterable<OpenOption> options) {
        new GzipCompressorInputStream(wrapped, true)
    }

    /** Given a list of options, create an appropriate {@link org.apache.commons.compress.compressors.gzip.GzipParameters}
     * configuration
     *
     * @param options NIO-style options of type {@link org.ysb33r.nio.provider.core.ProviderOpts}
     * @return GZIP parameters
     */
    static GzipParameters getParametersFromOptions(Iterable<OpenOption> options) {
        GzipParameters params = new GzipParameters()

        for (OpenOption opt in options) {

            try {
                ProviderOpts.FreeFormOpenOption namedOpt = (ProviderOpts.FreeFormOpenOption) opt

                switch (namedOpt.key) {
                    case 'gz.compression':
                    case 'org.ysb33r.nio.provider.gz.compression':
                        params.setCompressionLevel((Integer) namedOpt.value)
                        break
                }
            }
            catch (ClassCastException) {
                // Ignore any options, not of that type.
            }
        }
        params
    }

    private static final List<String> OPTION_NAMES = [
        'compression'
    ]


}
