/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.gz

import groovy.transform.CompileStatic
import org.apache.commons.compress.compressors.CompressorStreamFactory
import org.ysb33r.nio.provider.commons_core.AbstractCompressorFileSystem
import org.ysb33r.nio.provider.core.FileStoreProvider
import org.ysb33r.nio.provider.gz.internal.Builder
import java.nio.file.OpenOption
/** Represents a filesystem for file compressed with GZIP.
 *
 * @since 0.1
 */
@CompileStatic
class GzFileSystem extends AbstractCompressorFileSystem {

    /** Creates an instance of @schemeName@@-based filesystem.
     *
     * @param provider The filesystem provider that is creating this filesystem
     * @param fileStoreProvider A way to the filestores for the filesystem that underlays this compressed filesystem.
     * @param env Additional options for creating the filesystem.
     */
    protected GzFileSystem(
        GzFileSystemProvider provider,
        FileStoreProvider fileStoreProvider,
        Map<String,?> env
    ) {
        super(provider,fileStoreProvider, env)
    }

    /** Create a compressed output stream over another stream.
     *
     * @param wrapped Output stream that is wrapped to create a compressed output stream.
     * @param options Compression options. Can be empty. Never {@code null}.
     * @return Compressing output stream
     */
    @Override
    protected OutputStream createOutputStreamOver(OutputStream secondaryStream, Iterable<OpenOption> compressionOptions) {
        Builder.buildOutputStream(secondaryStream, compressionOptions)
    }

    /** Create a compressed input stream over another stream.
     *
     * @param wrapped Input stream that is wrapped to create a compressed input stream.
     * @param options Compression options. Can be empty. Never {@code null}.
     * @return Decompressing input stream.
     */
    @Override
    protected InputStream createInputStreamOver(InputStream wrapped, Iterable<OpenOption> options) {
        Builder.buildInputStream(wrapped, options)
    }

    /** List of compression options supported by this filesystem.
     *
     * @return Compression options. Can be empty, but never {@code null}.}
     */
    @Override
    Iterable<String> getOptionNames() {
        Builder.optionNames
    }
}
