/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.bz2.internal

import groovy.transform.CompileStatic
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream
import org.ysb33r.nio.provider.core.ProviderOpts

import java.nio.file.OpenOption

/** Option and stream builder for BZIP2.
 *
 * @since 0.1
 */
@CompileStatic
class Builder {

    /** A list of short option names that are valid of BZIP2.
     *
     * @return List of valid short option names
     */
    static Iterable<String> getOptionNames() {
        OPTION_NAMES
    }

    static OutputStream buildOutputStream(OutputStream secondaryStream, Iterable<OpenOption> compressionOptions) {
        Integer blocksize = getBlocksizeFromOptions(compressionOptions)
        blocksize ? new BZip2CompressorOutputStream(secondaryStream, blocksize) : new BZip2CompressorOutputStream(secondaryStream)
    }

    static InputStream buildInputStream(InputStream secondaryStream, Iterable<OpenOption> compressionOptions) {
        new BZip2CompressorInputStream(secondaryStream, true)
    }

    /** Given a list of options, try to find a blocksize.
     *
     * @param options NIO-style options of type {@link org.ysb33r.nio.provider.core.ProviderOpts}
     * @return Blocksize if provided otherwise {@code null}
     */
    static Integer getBlocksizeFromOptions(Iterable<OpenOption> options) {

        for (OpenOption opt in options) {

            try {
                ProviderOpts.FreeFormOpenOption namedOpt = (ProviderOpts.FreeFormOpenOption) opt

                switch (namedOpt.key) {
                    case 'bz2.compression':
                    case 'org.ysb33r.nio.provider.bz2.compression':
                        return namedOpt.value.toString().toInteger()
                        break
                }
            }
            catch (ClassCastException) {
                // Ignore any options, not of that type.
            }
        }

        null
    }

    private static final List<String> OPTION_NAMES = [
        'compression'
    ]


}
