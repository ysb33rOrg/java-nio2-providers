/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.xz

import groovy.transform.CompileStatic

import static org.ysb33r.nio.provider.core.ProviderOpts.FreeFormOpenOption

/** Options for opening a XZ stream for reading or writing.
 *
 * @since 0.1
 */
@CompileStatic
class OpenOptions {
    static FreeFormOpenOption XZ_COMPRESSION(int level) {
        new FreeFormOpenOption('org.ysb33r.nio.provider.xz.compression', level)
    }

    static FreeFormOpenOption XZ_MEMLIMITKB(int limit) {
        new FreeFormOpenOption('org.ysb33r.nio.provider.xz.memlimitkb', limit)
    }
}
