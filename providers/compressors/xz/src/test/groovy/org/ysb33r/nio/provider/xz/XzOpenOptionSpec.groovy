/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.xz

import spock.lang.Unroll

import java.nio.file.Path

import static org.ysb33r.nio.provider.xz.OpenOptions.XZ_COMPRESSION
import static org.ysb33r.nio.provider.xz.OpenOptions.XZ_MEMLIMITKB

class XzOpenOptionSpec extends XzSpecification {

    static final String contents = 'contents'

    @Unroll
    void "Writing to a compressed file with option #testThisOpenOption"() {
        given:
        Path testPath = rwCompressedPath()

        when:
        String data = writeDataToFileWithOptionsAndReadback(testPath, testThisOpenOption, contents)

        then:
        data == contents

        cleanup:
        testPath?.fileSystem?.close()

        where:
        testThisOpenOption << [XZ_COMPRESSION(9)]
    }

    @Unroll
    void "Reading from a compressed file with option #testThisOpenOption"() {
        given:
        Path testPath = roCompressedPath()

        when:
        String data = readContentsFromCreatedCompressedPath(withOption, testPath)

        then:
        data.startsWith(contents)

        cleanup:
        testPath?.fileSystem?.close()

        where:
        withOption << [XZ_MEMLIMITKB(100000)]
    }

}