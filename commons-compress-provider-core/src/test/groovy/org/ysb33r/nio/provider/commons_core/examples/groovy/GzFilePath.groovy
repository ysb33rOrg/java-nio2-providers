/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.commons_core.examples.groovy

import groovy.transform.CompileStatic
import org.ysb33r.nio.provider.commons_core.AbstractCompressorFilePath

import java.nio.file.Path

@CompileStatic
class GzFilePath extends AbstractCompressorFilePath {

    // tag::c-tor[]
    GzFilePath(GzFileSystem fs, Path fileServiceLayer) {
        super(fs, fileServiceLayer, GzFilePath) // <1>
    }
    // end::c-tor[]
}
