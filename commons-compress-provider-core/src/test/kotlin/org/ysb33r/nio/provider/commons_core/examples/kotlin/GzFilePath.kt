/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.commons_core.examples.kotlin

import org.ysb33r.nio.provider.commons_core.AbstractCompressorFilePath
import java.nio.file.Path

class GzFilePath : AbstractCompressorFilePath {
    // tag::c-tor[]
    constructor(fs: GzFileSystem, fileServiceLayer: Path) : super(fs, fileServiceLayer, GzFilePath::class.java) // <1>
    // end::c-tor[]
}