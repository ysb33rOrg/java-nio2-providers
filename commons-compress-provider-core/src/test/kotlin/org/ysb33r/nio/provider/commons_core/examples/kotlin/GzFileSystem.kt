/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.commons_core.examples.kotlin

import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream
import org.apache.commons.compress.compressors.gzip.GzipParameters
import org.ysb33r.nio.provider.commons_core.AbstractCompressorFileSystem
import org.ysb33r.nio.provider.core.FileStoreProvider
import org.ysb33r.nio.provider.core.ProviderOpts
import java.io.InputStream
import java.io.OutputStream
import java.nio.file.OpenOption

class GzFileSystem : AbstractCompressorFileSystem {
    // tag::implementation[]
    constructor(provider: GzFileSystemProvider, fileStoreProvider: FileStoreProvider, env: MutableMap<String, *>) : // <1>
        super(provider, fileStoreProvider, env)

    override fun getOptionNames(): Iterable<String> {
        return listOf<String>("gz.compression")
    }

    override fun createInputStreamOver(wrapped: InputStream, options: MutableIterable<OpenOption>?): InputStream {
        return GzipCompressorInputStream(wrapped, true) // <2>
    }

    override fun createOutputStreamOver(wrapped: OutputStream?, options: MutableIterable<OpenOption>): OutputStream {
        val params = GzipParameters() // <3>
        for (opt in options) { // <4>
            try {
                val namedOpt = opt as ProviderOpts.FreeFormOpenOption
                when (namedOpt.key) {
                    "gz.compression" -> params.compressionLevel = namedOpt.value as Int // <5>
                }
            } catch (e: ClassCastException) {

            }
        }
        return GzipCompressorOutputStream(wrapped, params) // <6>
    }
    // end::implementation[]
}
