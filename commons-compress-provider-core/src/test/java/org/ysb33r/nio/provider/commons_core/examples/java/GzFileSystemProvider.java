/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.commons_core.examples.java;

import org.ysb33r.nio.provider.commons_core.AbstractCompressorFileSystemProvider;

import java.io.IOException;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.attribute.FileAttribute;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class GzFileSystemProvider extends AbstractCompressorFileSystemProvider {

    // tag::c-tor[]
    public GzFileSystemProvider() {
        super("gz", GzFileSystem.class, GzFilePath.class); // <1>
    }
    // end::c-tor[]

    // tag::methods[]
    @Override
    protected Set<Class> getSupportedAttributeViews() {
        return new HashSet<Class>(); // <2>
    }

    @Override
    protected Map<String, Class> getSupportedAttributeViewNames() {
        return new HashMap<String, Class>(); // <3>
    }
    // end::methods[]
}