/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.commons_core.examples.java;

import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipParameters;
import org.ysb33r.nio.provider.commons_core.AbstractCompressorFileSystem;
import org.ysb33r.nio.provider.core.FileStoreProvider;
import org.ysb33r.nio.provider.core.ProviderOpts;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.OpenOption;
import java.util.ArrayList;
import java.util.Map;

public class GzFileSystem extends AbstractCompressorFileSystem {

    // tag::implementation[]
    public GzFileSystem(GzFileSystemProvider provider, FileStoreProvider fileStoreProvider, Map<String, ?> env) { // <1>
        super(provider, fileStoreProvider, env);
    }

    @Override
    protected Iterable<String> getOptionNames() {
        return new ArrayList<String>() {
            {
                add("gz.compression");
            }
        };
    }

    @Override
    protected InputStream createInputStreamOver(InputStream wrapped, Iterable<OpenOption> options) throws IOException {
        return new GzipCompressorInputStream(wrapped, true); // <2>
    }


    @Override
    protected OutputStream createOutputStreamOver(OutputStream wrapped, Iterable<OpenOption> options) throws IOException {
        GzipParameters params = new GzipParameters(); // <3>
        for (OpenOption opt : options) { // <4>
            try {
                ProviderOpts.FreeFormOpenOption namedOpt = (ProviderOpts.FreeFormOpenOption) opt;
                switch (namedOpt.getKey()) {
                    case "gz.compression": // <5>
                        params.setCompressionLevel((Integer) namedOpt.getValue());
                        break;
                }
            } catch (ClassCastException e) {
            }
        }
        return new GzipCompressorOutputStream(wrapped, params); // <6>
    }
    // end::implementation[]
}
