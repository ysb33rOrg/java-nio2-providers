/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.commons_core.internal

import groovy.transform.CompileStatic
import org.ysb33r.nio.provider.commons_core.DecompressingReader

/**
 * @since
 */
@CompileStatic
class GlueReader implements DecompressingReader {

    GlueReader(InputStream decompressor,Closeable src) {
        this.decompressingStream = decompressor
        this.sourceStream = src
    }

    int read(byte[] b) {
        decompressingStream.read(b)
    }

    void close() {
        decompressingStream.close()
        sourceStream.close()
    }

    private InputStream decompressingStream
    private Closeable sourceStream
}
