/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.commons_core

import groovy.transform.CompileStatic
import groovy.transform.Synchronized
import groovy.util.logging.Slf4j
import org.apache.commons.compress.compressors.CompressorStreamFactory
import org.ysb33r.nio.provider.commons_core.internal.GlueReader
import org.ysb33r.nio.provider.commons_core.internal.GlueWriter
import org.ysb33r.nio.provider.core.FileStoreProvider
import org.ysb33r.nio.provider.core.ProviderOpts
import org.ysb33r.nio.provider.core.matcher.RegexMatcher

import java.io.FileSystem
import java.nio.file.*
import java.nio.file.attribute.BasicFileAttributeView
import java.nio.file.attribute.FileAttributeView
import java.nio.file.attribute.UserPrincipalLookupService
import java.util.concurrent.ConcurrentSkipListSet
import java.util.regex.PatternSyntaxException

/** Base class for all compressed filesystems implemented by Apache Commons Compress.
 *
 * Options supported by compressed filesystems:
 *
 * <ul>
 *   <li> org.ysb33r.nio.provider.XYZ.commons.memlimitKb - Maximum memory to allocate for streams
 *   <li> org.ysb33r.nio.provider.XYZ.commons.transferbufKb - The internal buffer size used to transfer data internally.
 * </ul>
 *
 * @since 0.1
 */
@Slf4j
@CompileStatic
abstract class AbstractCompressorFileSystem extends org.ysb33r.nio.provider.core.compressed.AbstractCompressedFileSystem {

    /** Compressed filesystems allow read and write access.
     */
    @Override
    boolean isReadOnly() {
        return false
    }

    /**
     * Returns an object to iterate over the paths of the root directories.
     *
     * @return An empty list as compressed filesystem do not have the concept of directories.
     */
    @Override
    Iterable<Path> getRootDirectories() {
        ROOTS
    }

    /**
     * Returns the set of the {@link FileAttributeView#name names} of the file
     * attribute views supported by this {@code FileSystem}.
     *
     * <p> The {@link BasicFileAttributeView} is required to be supported and
     * therefore the set contains at least one element, "basic".
     *
     * <p> The {@link FileStore#supportsFileAttributeView(String)
     * supportsFileAttributeView(String)} method may be used to test if an
     * underlying {@link FileStore} supports the file attributes identified by a
     * file attribute view.
     *
     * @return An unmodifiable set of the names of the supported file attribute
     *          views
     */
    @Override
    Set<String> supportedFileAttributeViews() {
        ['basic'] as Set<String>
    }

    /**
     * Returns a {@code PathMatcher} that performs match operations on the
     * {@code String} representation of {@link Path} objects by interpreting a
     * given pattern.
     *
     * The {@code syntaxAndPattern} parameter identifies the syntax and the
     * pattern and takes the form:
     * <blockquote><pre>
     * <i>syntax</i><b>:</b><i>pattern</i>
     * </pre></blockquote>
     * where {@code ':'} stands for itself.
     *
     * <p> A {@code FileSystem} implementation supports the "{@code glob}" and
     * "{@code regex}" syntaxes, and may support others. The value of the syntax
     * component is compared without regard to case.
     *
     * <p> When the syntax is "{@code glob}" then the {@code String}
     * representation of the path is matched using a limited pattern language
     * that resembles regular expressions but with a simpler syntax. For example:
     *
     * <blockquote>
     * <table border="0" summary="Pattern Language">
     * <tr>
     *   <td>{@code *.java}</td>
     *   <td>Matches a path that represents a file name ending in {@code .java}</td>
     * </tr>
     * <tr>
     *   <td>{@code *.*}</td>
     *   <td>Matches file names containing a dot</td>
     * </tr>
     * <tr>
     *   <td>{@code *.{java,class}}</td>
     *   <td>Matches file names ending with {@code .java} or {@code .class}</td>
     * </tr>
     * <tr>
     *   <td>{@code foo.?}</td>
     *   <td>Matches file names starting with {@code foo.} and a single
     *   character extension</td>
     * </tr>
     * <tr>
     *   <td><tt>&#47;home&#47;*&#47;*</tt>
     *   <td>Matches <tt>&#47;home&#47;gus&#47;data</tt> on UNIX platforms</td>
     * </tr>
     * <tr>
     *   <td><tt>&#47;home&#47;**</tt>
     *   <td>Matches <tt>&#47;home&#47;gus</tt> and
     *   <tt>&#47;home&#47;gus&#47;data</tt> on UNIX platforms</td>
     * </tr>
     * <tr>
     *   <td><tt>C:&#92;&#92;*</tt>
     *   <td>Matches <tt>C:&#92;foo</tt> and <tt>C:&#92;bar</tt> on the Windows
     *   platform (note that the backslash is escaped; as a string literal in the
     *   Java Language the pattern would be <tt>"C:&#92;&#92;&#92;&#92;*"</tt>) </td>
     * </tr>
     *
     * </table>
     * </blockquote>
     *
     * <p> The following rules are used to interpret glob patterns:
     *
     * <ul>
     *   <li><p> The {@code *} character matches zero or more {@link Character
     *   characters} of a {@link Path#getName(int) name} component without
     *   crossing directory boundaries. </p></li>
     *
     *   <li><p> The {@code **} characters matches zero or more {@link Character
     *   characters} crossing directory boundaries. </p></li>
     *
     *   <li><p> The {@code ?} character matches exactly one character of a
     *   name component.</p></li>
     *
     *   <li><p> The backslash character ({@code \}) is used to escape characters
     *   that would otherwise be interpreted as special characters. The expression
     * {@code \\} matches a single backslash and "\{" matches a left brace
     *   for example.  </p></li>
     *
     *   <li><p> The {@code [ ]} characters are a <i>bracket expression</i> that
     *   match a single character of a name component out of a set of characters.
     *   For example, {@code [abc]} matches {@code "a"}, {@code "b"}, or {@code "c"}.
     *   The hyphen ({@code -}) may be used to specify a range so {@code [a-lz]}
     *   specifies a range that matches from {@code "a"} to {@code "lz"} (inclusive).
     *   These forms can be mixed so [abce-g] matches {@code "a"}, {@code "b"},
     * {@code "c"}, {@code "e"}, {@code "f"} or {@code "g"}. If the character
     *   after the {@code [} is a {@code !} then it is used for negation so {@code
     * [!a-c]} matches any character except {@code "a"}, {@code "b"}, or {@code
     *   "c"}.
     *   <p> Within a bracket expression the {@code *}, {@code ?} and {@code \}
     *   characters match themselves. The ({@code -}) character matches itself if
     *   it is the first character within the brackets, or the first character
     *   after the {@code !} if negating.</p></li>
     *
     *   <li><p> The {@code{}} characters are a group of subpatterns, where
     *   the group matches if any subpattern in the group matches. The {@code ","}
     *   character is used to separate the subpatterns. Groups cannot be nested.
     *   </p></li>
     *
     *   <li><p> Leading period<tt>&#47;</tt>dot characters in file name are
     *   treated as regular characters in match operations. For example,
     *   the {@code "*"} glob pattern matches file name {@code ".login"}.
     *   The {@link Files#isHidden} method may be used to test whether a file
     *   is considered hidden.
     *   </p></li>
     *
     *   <li><p> All other characters match themselves in an implementation
     *   dependent manner. This includes characters representing any {@link
     * FileSystem # getSeparator name-separators}. </p></li>
     *
     *   <li><p> The matching of {@link Path#getRoot root} components is highly
     *   implementation-dependent and is not specified. </p></li>
     *
     * </ul>
     *
     * <p> When the syntax is "{@code regex}" then the pattern component is a
     * regular expression as defined by the {@link java.util.regex.Pattern}
     * class.
     *
     * <p>  For both the glob and regex syntaxes, the matching details, such as
     * whether the matching is case sensitive, are implementation-dependent
     * and therefore not specified.
     *
     * @param syntaxAndPattern
     *          The syntax and pattern
     *
     * @return A path matcher that may be used to match paths against the pattern
     *
     * @throws IllegalArgumentException
     *          If the parameter does not take the form: {@code syntax:pattern}
     * @throws java.util.regex.PatternSyntaxException
     *          If the pattern is invalid
     * @throws UnsupportedOperationException
     *          If the pattern syntax is not known to the implementation
     *
     * @see Files#newDirectoryStream(Path, String)
     */
    @Override
    PathMatcher getPathMatcher(String syntaxAndPattern) {
        String[] parts = syntaxAndPattern.split(':',2)

        if(parts.size() != 2) {
            throw new IllegalArgumentException('Path matcher pattern needs to be of form syntax:pattern')
        }

        try {
            switch(parts[0]) {
                case 'regex':
                    new RegexMatcher(parts[1])
                    break
                default:
                    throw new UnsupportedOperationException("'${parts[0]}' is not a supported syntax of scheme '${provider().scheme}'")
            }
        } catch(UnsupportedOperationException|PatternSyntaxException e) {
            throw e
        } catch(final Exception e) {
            throw new UnsupportedOperationException("Something went wrong creating a pattern matcher", e)
        }
    }

    /** Compressed filesystem do not support lookup services.
     *
     * @throws UnsupportedOperationException
     */
    @Override
    UserPrincipalLookupService getUserPrincipalLookupService() {
        throw new UnsupportedOperationException('Compressed filesystems do not support lookup services. It might be posible to query the locate a lookup service for the filesystem where the raw compressed file is located.')
    }

    /** Compressed filesystem do not support watch services.
     *
     * @throws UnsupportedOperationException
     */
    @Override
    WatchService newWatchService() throws IOException {
        throw new UnsupportedOperationException('Compressed filesystems do not support watch services. It might be posible to query the locate a watch service for the filesystem where the raw compressed file is located.')
    }

    /** Retuns a ful-qualified property name that will be recognised by providers implemented using Apache Commons Compress.
     *
     * @param shortName Short property name
     * @return A name that can be passed to an options map or read as a System property.
     *
     * @since 0.1
     */
    String fqPropName(String shortName) {
        "org.ysb33r.nio.provider.${provider().getScheme()}.commons.${shortName}"
    }

    /** The {@link org.apache.commons.compress.compressors.CompressorStreamFactory}
     * associated with this filesystem
     *
     * @since 0.1
     */
    CompressorStreamFactory getStreamFactory() {
        this.streamFactory
    }

    /** Size of buffer used to transferring data internally between an Apache Commons Compress
     * {@link org.apache.commons.compress.compressors.CompressorInputStream /
     * org.apache.commons.compress.compressors.CompressorOutputStream and a {@link java.nio.ByteBuffer}.
     *
     * @return Buffer size in bytes
     *
     * @since 0.1
     */
    int getCompressedStreamTransferSize() {
        this.compressedStreamTransferSize
    }

    /** Registers a data channel with this filesystem. This is primarily for use
     * when a filesystem is clsoed so that all associated channels are also closed.
     *
     * @param channel
     */
    void register(CompressedFileSeekableByteChannel channel) {
        channels.add(channel)
    }

    /** Removes a data channel from this filesystem.
     *
     * @param channel
     */
    void deregister(CompressedFileSeekableByteChannel channel) {
        channels.remove(channel)
    }

    /** Exracts options that are specific to this filesystem type.
     *
     * @return Redacted set of options.
     */
    @Override
    Set<OpenOption> getCompressionSpecificOptions(Iterable<OpenOption> options) {
        final String scheme = provider().getScheme()
        Iterable<String> validNames = getOptionNames()
        options.findAll { OpenOption opt ->
            if (opt instanceof ProviderOpts.FreeFormOpenOption) {
                String name = ((ProviderOpts.FreeFormOpenOption) opt).getKey()
                validNames.find { String optName ->
                    name == "${scheme}.${optName}" || name == "org.ysb33r.nio.provider.${scheme}.${optName}".toString()
                }
            } else {
                false
            }
        } as Set<OpenOption>
    }

    /** Creates an input stream that will decompress data being fed to it using the algorithm associated with the
     *   specific filesystem.
     *
     * @param path File path on compressed filesystem,
     * @param options Set of options passed at channel creation request
     * @return Decompressing input stream
     */
    DecompressingReader getDecompressingReader(AbstractCompressorFilePath path, Set<? extends OpenOption> options) {
        Set<OpenOption> compressionOptions = getCompressionSpecificOptions(options)
        Set<? extends OpenOption> tmpOptions = []
        tmpOptions.addAll(options)
        tmpOptions.removeAll(compressionOptions)
        InputStream wrapped = Files.newInputStream(path.serviceLayerPath, (tmpOptions.toArray() as OpenOption[]))
        InputStream decompressor = createInputStreamOver(wrapped, compressionOptions)
        new GlueReader(decompressor, wrapped)
    }

    /** Creates an output stream that will compress data being fed to it using the algorithm associated with the
     *   specific filesystem.
     *
     * @param path File path on compressed filesystem,
     * @param options Set of options passed at channel creation request
     * @return Compressing output stream
     */
    CompressingWriter getCompressingWriter(AbstractCompressorFilePath path, Set<? extends OpenOption> options) {
        Iterable<OpenOption> compressionOptions = getCompressionSpecificOptions(options)
        Set<? extends OpenOption> tmpOptions = []
        tmpOptions.addAll(options)
        tmpOptions.removeAll(compressionOptions)
        OutputStream wrapped = Files.newOutputStream(path.serviceLayerPath, (tmpOptions.toArray() as OpenOption[]))
        OutputStream compressor = createOutputStreamOver(wrapped, compressionOptions)
        new GlueWriter(compressor, wrapped)
    }

    /** Set specific settings that will be used to configure compressor from Apache Commons Compress.
     *
     * In this implementation it will interrogate the following settings:
     *
     * <ul>
     *     <li> org.ysb33r.nio.provider.XYZ.commons.memlimitKb
     *     <li> org.ysb33r.nio.provider.XYZ.commons.transferbufKb
     * </ul>
     *
     * @param provider The provider that is creating this filesystem.
     * @param fileStoreProvider A way of obtaining the filestores for this filesystem.
     * @param env Settings that can be interrogated during construction.
     */
    protected AbstractCompressorFileSystem(AbstractCompressorFileSystemProvider provider, FileStoreProvider fileStoreProvider, Map<String, ?> env) {
        super(provider, fileStoreProvider)
        String key = fqPropName('memlimitKb')
        Integer memlimit = mapGetInteger(env, key, System.getProperty(key))

        String transferKey = fqPropName('transferbufKb')
        Integer transferSize = mapGetInteger(env, transferKey, System.getProperty(transferKey)) ?: 1

        streamFactory = memlimit ? new CompressorStreamFactory(true, memlimit) : new CompressorStreamFactory(true)

        compressedStreamTransferSize = transferSize << 10
    }

    /** Closes the filesystem.
     */
    @Override
    protected void doCloseFileSystem() {
    }

    /** This is a NOOP for compressed filesystems as directories are not supported.
     *
     */
    protected void doCloseDirectoryStreams() {
    }

    /** This is a NOOP for compressed filesystems as watch services are not supported in this implementation.
     *
     */
    protected void doCloseWatchServices() {
    }

    /** Closes all of the curretly registered channels.
     *
     */
    protected void doCloseChannels() {
        closeChannelsSynchronized()
    }

    /** Gets a value from a map or use a default value
     *
     * @param map
     * @param key
     * @param defaultValue
     * @return Value. Can be null
     */
    protected Object mapGet(final Map<String, ?> map, final String key, final Object defaultValue) {
        if (map.containsKey(key)) {
            map[key]
        } else {
            defaultValue
        }
    }

    protected Integer mapGetInteger(final Map<String, ?> map, final String key, final Object defaultValue) {
        mapGet(map, key, defaultValue)?.toString()?.toInteger()
    }

    @Synchronized
    private void closeChannelsSynchronized() {
        while (!channels.empty) {
            try {
                channels.pollFirst()?.close()
            } catch (Exception e) {
                log.debug "Exception during channel closure: ${e.message}"
            }
        }
    }

    private final CompressorStreamFactory streamFactory
    private int compressedStreamTransferSize
    private
    final ConcurrentSkipListSet<CompressedFileSeekableByteChannel> channels = new ConcurrentSkipListSet<CompressedFileSeekableByteChannel>(new Comparator<CompressedFileSeekableByteChannel>() {
        @Override
        int compare(CompressedFileSeekableByteChannel o1, CompressedFileSeekableByteChannel o2) {
            return o1.hashCode() <=> o2.hashCode()
        }
    })

    /** Create a compressed output stream over another stream.
     *
     * @param wrapped Output stream that is wrapped to create a compressed output stream.
     * @param options Compression options. Can be empty. Never {@code null}.
     * @return Compressing output stream
     */
    abstract protected OutputStream createOutputStreamOver(OutputStream wrapped, Iterable<OpenOption> options) throws IOException

    /** Create a compressed input stream over another stream.
     *
     * @param wrapped Input stream that is wrapped to create a compressed input stream.
     * @param options Compression options. Can be empty. Never {@code null}.
     * @return Decompressing input stream.
     */
    abstract protected InputStream createInputStreamOver(InputStream wrapped, Iterable<OpenOption> options) throws IOException

    /** List of compression options supported by this filesystem.
     *
     * @return Compression options. Can be empty, but never {@code null}.}
     */
    abstract protected Iterable<String> getOptionNames()

    static final List<String> NO_COMPRESSION_OPTIONS = ((List<String>) []).asImmutable()
    private static final List<Path> ROOTS = ((List<Path>) []).asImmutable()
}
