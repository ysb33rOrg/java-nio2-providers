/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.commons_core

import groovy.transform.CompileStatic

import java.nio.ByteBuffer
import java.nio.channels.*
import java.nio.file.OpenOption
import java.nio.file.attribute.FileAttribute

/** Implementation of a {@link java.nio.channels.SeekableByteChannel} specifically for
 * compressed filesystem as implemented by Apache Commons Compress.
 *
 * @since 0.1
 */
@CompileStatic
class CompressedFileSeekableByteChannel implements SeekableByteChannel, WritableByteChannel {

    /** Creates a readable channel.
     *
     * @param path Path to compressed file.
     * @param options Options that are conducive to reading a file. Additional optional options for the secondary layer
     * that actually serves the compressed file are also allowed.
     * @return Readable channel
     *
     * @since 0.1
     */
    static CompressedFileSeekableByteChannel createReadableChannel(AbstractCompressorFilePath path, Set<? extends OpenOption> options) {
        AbstractCompressorFileSystem fs = ((AbstractCompressorFileSystem) path.fileSystem)
        DecompressingReader strm = fs.getDecompressingReader(path, options)
        new CompressedFileSeekableByteChannel(strm, fs.compressedStreamTransferSize, fs)
    }

    /** Creates a writeable channel.
     *
     * @param path Path to compressed file.
     * @param options Options that are conducive to reading a file. Additional optional options for the secondary layer
     * that actually serves the compressed file are also allowed.
     * @return Readable channel
     *
     * @since 0.1
     */
    static CompressedFileSeekableByteChannel createWriteableChannel(AbstractCompressorFilePath path, Set<? extends OpenOption> options, FileAttribute<?>[] attrs) {
        AbstractCompressorFileSystem fs = ((AbstractCompressorFileSystem) path.fileSystem)
        CompressingWriter strm = fs.getCompressingWriter(path, options)
        new CompressedFileSeekableByteChannel(strm, fs.compressedStreamTransferSize, fs)
    }

    /**
     * Reads a sequence of bytes from this channel into the given buffer.
     *
     * <p> Bytes are read starting at this channel's current position, and
     * then the position is updated with the number of bytes actually read.
     * Otherwise this method behaves exactly as specified in the {@link
     * java.nio.channels.ReadableByteChannel} interface.
     */
    @Override
    int read(ByteBuffer dst) throws IOException {
        if (!isOpen()) {
            throw new ClosedChannelException()
        }
        if (reader == null) {
            throw new NonReadableChannelException()
        }

        int got = reader.read(buffer)

        if (got > -1) {
            position += got
            dst.put(buffer, 0, got)
        }
        return got

    }

    /**
     * Writes a sequence of bytes to this channel from the given buffer.
     *
     * <p> Bytes are written starting at this channel's current position, unless
     * the channel is connected to an entity such as a file that is opened with
     * the {@link java.nio.file.StandardOpenOption#APPEND APPEND} option, in
     * which case the position is first advanced to the end. The entity to which
     * the channel is connected is grown, if necessary, to accommodate the
     * written bytes, and then the position is updated with the number of bytes
     * actually written. Otherwise this method behaves exactly as specified by
     * the {@link java.nio.channels.WritableByteChannel} interface.
     */
    @Override
    int write(ByteBuffer src) throws IOException {
        if (!isOpen()) {
            throw new ClosedChannelException()
        }
        if (writer == null) {
            throw new NonWritableChannelException()
        }
        int remaining = src.remaining()
        if (remaining == 0) {
            return 0
        }
        int got = 0
        if (buffer.length > remaining) {
            src.get(buffer, 0, remaining)
            got = remaining
        } else {
            src.get(buffer)
            got = buffer.length
        }

        writer.write(buffer, 0, got)
        position += got
        return got
    }

    /**
     * Returns this channel's position.
     *
     * @return This channel's position,
     *          a non-negative integer counting the number of bytes
     *          from the beginning of the entity to the current position
     *
     * @throws ClosedChannelException
     *          If this channel is closed
     * @throws IOException
     *          If some other I/O error occurs
     */
    @Override
    long position() throws IOException {
        if (!isOpen()) {
            throw new ClosedChannelException()
        }
        this.position
    }

    /**
     * Sets this channel's position.
     *
     * <p> Setting the position to a value that is greater than the current size
     * is legal but does not change the size of the entity.  A later attempt to
     * read bytes at such a position will immediately return an end-of-file
     * indication.  A later attempt to write bytes at such a position will cause
     * the entity to grow to accommodate the new bytes; the values of any bytes
     * between the previous end-of-file and the newly-written bytes are
     * unspecified.
     *
     * <p> Setting the channel's position is not recommended when connected to
     * an entity, typically a file, that is opened with the {@link
     * java.nio.file.StandardOpenOption # APPEND APPEND} option. When opened for
     * append, the position is first advanced to the end before writing.
     *
     * @param newPosition
     *         The new position, a non-negative integer counting
     *         the number of bytes from the beginning of the entity
     *
     * @return This channel
     *
     * @throws ClosedChannelException
     *          If this channel is closed
     * @throws IllegalArgumentException
     *          If the new position is negative
     * @throws IOException
     *          If some other I/O error occurs
     */
    @Override
    SeekableByteChannel position(long newPosition) throws IOException {
        if (!isOpen()) {
            throw new ClosedChannelException()
        }
        throw new InvalidIOCompressedFilesystemException("Cannot change position on a compressed stream")
    }

    /**
     * Returns the current size of entity to which this channel is connected.
     *
     * @return Always -1 as the size of a compressed file cannot accurately be reported.
     *
     * @throws ClosedChannelException
     *          If this channel is closed
     * @throws IOException
     *          If some other I/O error occurs
     */
    @Override
    long size() throws IOException {
        if (!isOpen()) {
            throw new ClosedChannelException()
        }
        -1
    }

    /**
     * Truncates the entity, to which this channel is connected, to the given
     * size.
     *
     * <p> If the given size is less than the current size then the entity is
     * truncated, discarding any bytes beyond the new end. If the given size is
     * greater than or equal to the current size then the entity is not modified.
     * In either case, if the current position is greater than the given size
     * then it is set to that size.
     *
     * <p> An implementation of this interface may prohibit truncation when
     * connected to an entity, typically a file, opened with the {@link
     * java.nio.file.StandardOpenOption # APPEND APPEND} option.
     *
     * @param size
     *         The new size, a non-negative byte count
     *
     * @return This channel
     *
     * @throws NonWritableChannelException
     *          If this channel was not opened for writing
     * @throws ClosedChannelException
     *          If this channel is closed
     * @throws IllegalArgumentException
     *          If the new size is negative
     * @throws IOException
     *          If some other I/O error occurs
     */
    @Override
    SeekableByteChannel truncate(long size) throws IOException {
        if (!isOpen()) {
            throw new ClosedChannelException()
        }

        throw new InvalidIOCompressedFilesystemException("Channels on compressed filesystems cannot be truncated")
    }

    /**
     * Tells whether or not this channel is open.
     *
     * @return <tt>true</tt> if, and only if, this channel is open
     */
    @Override
    boolean isOpen() {
        reader != null || writer != null
    }

    /**
     * Closes this channel.
     *
     * <p> After a channel is closed, any further attempt to invoke I/O
     * operations upon it will cause a {@link ClosedChannelException} to be
     * thrown.
     *
     * <p> If this channel is already closed then invoking this method has no
     * effect.
     *
     * <p> This method may be invoked at any time.  If some other thread has
     * already invoked it, however, then another invocation will block until
     * the first invocation is complete, after which it will return without
     * effect. </p>
     *
     * @throws IOException  If an I/O error occurs
     */
    @Override
    void close() throws IOException {

        if (!isOpen()) {
            return
        }

        if (reader != null) {
            reader.close()
            reader = null
        }
        if (writer != null) {
            writer.close()
            writer = null
        }

        fileSystem.deregister(this)
    }

    private CompressedFileSeekableByteChannel(
        DecompressingReader strm,
        int bufSize,
        AbstractCompressorFileSystem fs
    ) {
        reader = strm
        buffer = new byte[bufSize]
        fileSystem = fs
        fileSystem.register(this)
    }

    private CompressedFileSeekableByteChannel(
        CompressingWriter strm,
        int bufSize,
        AbstractCompressorFileSystem fs
    ) {
        writer = strm
        buffer = new byte[bufSize]
        fileSystem = fs
        fileSystem.register(this)
    }

    private long position = 0
    private DecompressingReader reader
    private CompressingWriter writer
    private byte[] buffer
    private final AbstractCompressorFileSystem fileSystem
}
