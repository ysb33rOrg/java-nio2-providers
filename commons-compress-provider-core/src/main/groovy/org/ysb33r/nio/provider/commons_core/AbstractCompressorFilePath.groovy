/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.commons_core

import groovy.transform.CompileStatic
import org.ysb33r.nio.provider.core.compressed.AbstractCompressedPath

import java.nio.file.Path

/** A base class to add specific file path functionality that can be deferred to functionality in Apache Commons Compress.
 *
 * @since 0.1
 */
@CompileStatic
abstract class AbstractCompressorFilePath extends AbstractCompressedPath {

    /** Pass-through constructor for all paths that are on compressed filesystems implemented
     * with Apache Commons Compress.
     *
     * @param fs Fileystem the path is attached to
     * @param fileServiceLayer The path that is the source for a compressed file.
     * @param pathClass The actual concrete class that represents a path of a compressed filesystem.
     */
    protected AbstractCompressorFilePath(org.ysb33r.nio.provider.core.compressed.AbstractCompressedFileSystem fs, Path fileServiceLayer, Class pathClass) {
        super(fs, fileServiceLayer, pathClass)
    }
}
