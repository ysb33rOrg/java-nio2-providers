/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.commons_core

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.ysb33r.nio.provider.core.AbstractPath
import org.ysb33r.nio.provider.core.FileStoreProvider
import org.ysb33r.nio.provider.core.compressed.AbstractCompressedFileSystem
import org.ysb33r.nio.provider.core.compressed.AbstractCompressedFileSystemProvider
import org.ysb33r.nio.provider.core.compressed.AbstractCompressedPath

import java.nio.channels.Channels
import java.nio.channels.SeekableByteChannel
import java.nio.file.*
import java.nio.file.attribute.FileAttribute
import java.nio.file.attribute.FileAttributeView

import static java.nio.file.StandardOpenOption.*

/** Base class for compressed filesystem providers that are implemented with Apache Commons Compress.
 *
 * @since 0.1
 */
@CompileStatic
@Slf4j
abstract class AbstractCompressorFileSystemProvider extends AbstractCompressedFileSystemProvider {

    /**
     * Opens or creates a file, returning a seekable byte channel to access the
     * file. This method works in exactly the manner specified by the {@link
     * Files # newByteChannel ( Path , Set , FileAttribute[] )} method.
     *
     * @param path
     *          the path to the file to open or create
     * @param options
     *          options specifying how the file is opened
     * @param attrs
     *          an optional list of file attributes to set atomically when
     *          creating the file
     *
     * @return a new seekable byte channel
     *
     * @throws IllegalArgumentException
     *          if the set contains an invalid combination of options
     * @throws UnsupportedOperationException
     *          if an unsupported open option is specified or the array contains
     *          attributes that cannot be set atomically when creating the file
     * @throws FileAlreadyExistsException
     *          if a file of that name already exists and the {@link
     * StandardOpenOption # CREATE_NEW CREATE_NEW} option is specified
     *          <i>(optional specific exception)</i>
     * @throws IOException
     *          if an I/O error occurs
     * @throws SecurityException
     *          In the case of the default provider, and a security manager is
     *          installed, the {@link SecurityManager#checkRead(String) checkRead}
     *          method is invoked to check read access to the path if the file is
     *          opened for reading. The {@link SecurityManager#checkWrite(String)
     *          checkWrite} method is invoked to check write access to the path
     *          if the file is opened for writing. The {@link
     * SecurityManager # checkDelete ( String ) checkDelete} method is
     *          invoked to check delete access if the file is opened with the
     * {@code DELETE_ON_CLOSE} option.
     */
    @Override
    SeekableByteChannel newByteChannel(Path path, Set<? extends OpenOption> options, FileAttribute<?>... attrs) throws IOException {
        Collection standardOptions = options.intersect(StandardOpenOption.values() as Set<? extends OpenOption>)
        if (standardOptions.empty) {
            standardOptions = [READ] as Set
        }
        Set<OpenOption> linkOptions = extractLinkOptions(options)
        Set<? extends OpenOption> redactedOptions = []

        if (standardOptions.contains(READ)) {
            Path realPath = path.toRealPath(linkOptions.toArray(new LinkOption[linkOptions.size()]))
            if (standardOptions.size() > 1) {
                throw new IllegalArgumentException("Cannot combine READ with any writeable option when scheme is '${getScheme()}'.")
            }
            redactedOptions.addAll(options)
            redactedOptions.removeAll(standardOptions)
            redactedOptions.add(READ as OpenOption)
            return CompressedFileSeekableByteChannel.createReadableChannel((AbstractCompressorFilePath) realPath, redactedOptions)

        } else if (standardOptions.contains(StandardOpenOption.WRITE)) {

            if (path.fileSystem.isReadOnly()) {
                throw new UnsupportedOperationException("Filesystem '${getScheme()}' is read-only")
            }

            if (standardOptions.contains(StandardOpenOption.APPEND)) {
                throw new UnsupportedOperationException("Cannot append to a compressed stream with scheme '${getScheme()}'")
            }

            if (standardOptions.contains(StandardOpenOption.DSYNC)) {
                log.warn("Ignoring DSYNC for scheme '${getScheme()}")
            }

            if (standardOptions.contains(StandardOpenOption.SPARSE)) {
                log.warn("Ignoring SPARSE for scheme '${getScheme()}")
            }

            if (standardOptions.contains(StandardOpenOption.TRUNCATE_EXISTING)) {
                log.warn("Ignoring TRUNCATE_EXISTING for scheme '${getScheme()}")
            }

            if (standardOptions.contains(StandardOpenOption.CREATE_NEW) && standardOptions.contains(StandardOpenOption.CREATE)) {
                throw new IllegalArgumentException("Cannot specify CREATE and CREATE_NEW together")
            }

            Boolean exists = Files.exists(path)

            if (standardOptions.contains(StandardOpenOption.CREATE_NEW) && exists) {
                throw new FileAlreadyExistsException("Cannot create ${path} as it already exists")
            }

            if (!exists && !standardOptions.contains(StandardOpenOption.CREATE) && !standardOptions.contains(StandardOpenOption.CREATE_NEW)) {
                throw new IllegalArgumentException("${path} does not exist, but CREATE/CREATE_NEW has not been specified")
            }

            return CompressedFileSeekableByteChannel.createWriteableChannel((AbstractCompressorFilePath) path, redactedOptions, attrs)

        } else {
            throw new UnsupportedOperationException("None of the supplied options are valid of a channel with scheme '${getScheme()}': Supplied [${options}]")
        }
    }

    /**
     * Opens or creates a file, returning an output stream that may be used to
     * write bytes to the file. This method works in exactly the manner
     * specified by the {@link Files#newOutputStream} method.
     *
     * <p> The default implementation of this method opens a channel to the file
     * as if by invoking the {@link #newByteChannel} method and constructs a
     * stream that writes bytes to the channel. This method should be overridden
     * where appropriate.
     *
     * @param path
     *          the path to the file to open or create
     * @param options
     *          options specifying how the file is opened
     *
     * @return a new output stream
     *
     * @throws IllegalArgumentException
     *          if {@code options} contains an invalid combination of options
     * @throws UnsupportedOperationException
     *          if an unsupported option is specified
     * @throws IOException
     *          if an I/O error occurs
     * @throws SecurityException
     *          In the case of the default provider, and a security manager is
     *          installed, the {@link SecurityManager#checkWrite(String) checkWrite}
     *          method is invoked to check write access to the file. The {@link
     * SecurityManager # checkDelete ( String ) checkDelete} method is
     *          invoked to check delete access if the file is opened with the
     * {@code DELETE_ON_CLOSE} option.
     */
    @Override
    OutputStream newOutputStream(Path path, OpenOption... options) throws IOException {
        Set<OpenOption> opts = options as Set
        if (opts.empty) {
            opts.add((OpenOption) WRITE)
            opts.add((OpenOption) CREATE)
        } else {
            if (opts.contains(READ)) {
                throw new IllegalArgumentException("READ is not allowed for an output stream")
            }
        }
        Channels.newOutputStream(newByteChannel(path, opts))
    }

/** Directories are not supported for compressed filesystems.
 *
 * <p> Always throws an exception.
 *
 * @throws UnsupportedOperationException
 */
    @Override
    void createDirectory(Path dir, FileAttribute<?>[] attrs) throws IOException {
        throw new UnsupportedOperationException("Directories are not supported for compressed filesystems")
    }

    /**
     * Deletes a file. This method works in exactly the  manner specified by the
     * {@link Files#delete} method.
     *
     * @param path
     *          the path to the file to delete
     *
     * @throws java.nio.file.NoSuchFileException
     *          if the file does not exist <i>(optional specific exception)</i>
     * @throws java.nio.file.DirectoryNotEmptyException
     *          if the file is a directory and could not otherwise be deleted
     *          because the directory is not empty <i>(optional specific
     *          exception)</i>
     * @throws IOException
     *          if an I/O error occurs
     * @throws SecurityException
     *          In the case of the default provider, and a security manager is
     *          installed, the {@link SecurityManager#checkDelete(String)} method
     *          is invoked to check delete access to the file
     */
    @Override
    void delete(Path path) throws IOException {
        Files.delete(((AbstractCompressedPath) path).serviceLayerPath)
    }

    /**
     * Copy a file to a target file. This method works in exactly the manner
     * specified by the {@link Files#copy(Path, Path, CopyOption [ ])} method
     * except that both the source and target paths must be associated with
     * this provider.
     *
     * @param source
     *          the path to the file to copy
     * @param target
     *          the path to the target file
     * @param options
     *          options specifying how the copy should be done
     *
     * @throws UnsupportedOperationException
     *          if the array contains a copy option that is not supported
     * @throws FileAlreadyExistsException
     *          if the target file exists but cannot be replaced because the
     * {@code REPLACE_EXISTING} option is not specified <i>(optional
     *          specific exception)</i>
     * @throws java.nio.file.DirectoryNotEmptyException
     *          the {@code REPLACE_EXISTING} option is specified but the file
     *          cannot be replaced because it is a non-empty directory
     *          <i>(optional specific exception)</i>
     * @throws IOException
     *          if an I/O error occurs
     * @throws SecurityException
     *          In the case of the default provider, and a security manager is
     *          installed, the {@link SecurityManager#checkRead(String) checkRead}
     *          method is invoked to check read access to the source file, the
     * {@link SecurityManager#checkWrite(String) checkWrite} is invoked
     *          to check write access to the target file. If a symbolic link is
     *          copied the security manager is invoked to check {@link
     * LinkPermission} {@code ( " symbolic " )}.
     */
    @Override
    void copy(Path source, Path target, CopyOption... options) throws IOException {

        if (source.fileSystem.provider() != target.fileSystem.provider()) {
            throw new UnsupportedOperationException("Source & target paths are not both from '${getScheme()}' scheme")
        }

        if (target.fileSystem.isReadOnly()) {
            throw new UnsupportedOperationException("Filesystem '${getScheme()} is read-only")
        }

        Files.copy(
            ((AbstractCompressedPath) source).serviceLayerPath,
            ((AbstractCompressedPath) target).serviceLayerPath,
            options
        )
    }

    /**
     * Move or rename a file to a target file. This method works in exactly the
     * manner specified by the {@link Files#move} method except that both the
     * source and target paths must be associated with this provider.
     *
     * @param source
     *          the path to the file to move
     * @param target
     *          the path to the target file
     * @param options
     *          options specifying how the move should be done
     *
     * @throws UnsupportedOperationException
     *          if the array contains a copy option that is not supported
     * @throws FileAlreadyExistsException
     *          if the target file exists but cannot be replaced because the
     * {@code REPLACE_EXISTING} option is not specified <i>(optional
     *          specific exception)</i>
     * @throws java.nio.file.DirectoryNotEmptyException
     *          the {@code REPLACE_EXISTING} option is specified but the file
     *          cannot be replaced because it is a non-empty directory
     *          <i>(optional specific exception)</i>
     * @throws java.nio.file.AtomicMoveNotSupportedException
     *          if the options array contains the {@code ATOMIC_MOVE} option but
     *          the file cannot be moved as an atomic file system operation.
     * @throws IOException
     *          if an I/O error occurs
     * @throws SecurityException
     *          In the case of the default provider, and a security manager is
     *          installed, the {@link SecurityManager#checkWrite(String) checkWrite}
     *          method is invoked to check write access to both the source and
     *          target file.
     */
    @Override
    void move(Path source, Path target, CopyOption... options) throws IOException {

        if (target.fileSystem.isReadOnly()) {
            throw new UnsupportedOperationException("Target filesystem '${getScheme()} is read-only")
        }

        AbstractCompressorFilePath srcPath = (AbstractCompressorFilePath) source
        AbstractCompressorFilePath destPath = (AbstractCompressorFilePath) target
        Files.move(srcPath.serviceLayerPath, destPath.serviceLayerPath, options)
    }

    /**
     * Returns a file attribute view of a given type. This method works in
     * exactly the manner specified by the {@link Files#getFileAttributeView}
     * method.
     *
     * <p> In the case of a compressed filesystem, it will first try look to see if
     * the compressed filesystem supports it directly, otherwise it will defer
     * to the secondary filesystem.
     *
     * @param path
     *          the path to the file
     * @param type
     *          the {@code Class} object corresponding to the file attribute view
     * @param options
     *          options indicating how symbolic links are handled
     *
     * @return a file attribute view of the specified type, or {@code null} if
     *          the attribute view type is not available
     */
    @Override
    def <V extends FileAttributeView> V getFileAttributeView(Path path, Class<V> type, LinkOption... options) {

        if (path instanceof AbstractCompressorFilePath) {

            final Set<Class> attributeViews = supportedAttributeViews

            if (attributeViews.contains(type)) {
                createFileAttributeView((AbstractCompressorFilePath) path, type, options)
            } else {
                Path serviceLayerPath = ((AbstractCompressorFilePath) path).serviceLayerPath
                serviceLayerPath.fileSystem.provider().getFileAttributeView(serviceLayerPath, type, options)
            }
        } else {
            log.debug "Attribute view of type ${type.name} was requested for a path of type ${path.class.name}. This is not a valid combination and results in a null return."
            null
        }
    }

    /**
     * Sets the value of a file attribute. This method works in exactly the
     * manner specified by the {@link Files#setAttribute} method.
     *
     * @param path
     *          the path to the file
     * @param attribute
     *          the attribute to set
     * @param value
     *          the attribute value
     * @param options
     *          options indicating how symbolic links are handled
     *
     * @throws UnsupportedOperationException
     *          if the attribute view is not available
     * @throws IllegalArgumentException
     *          if the attribute name is not specified, or is not recognized, or
     *          the attribute value is of the correct type but has an
     *          inappropriate value
     * @throws ClassCastException
     *          If the attribute value is not of the expected type or is a
     *          collection containing elements that are not of the expected
     *          type
     * @throws IOException
     *          If an I/O error occurs
     * @throws SecurityException
     *          In the case of the default provider, and a security manager is
     *          installed, its {@link SecurityManager#checkWrite(String) checkWrite}
     *          method denies write access to the file. If this method is invoked
     *          to set security sensitive attributes then the security manager
     *          may be invoked to check for additional permissions.
     */
    @Override
    void setAttribute(Path path, String attribute, Object value, LinkOption... options) throws IOException {
        final String viewName = getViewNameFromAttribute(attribute)

        if (viewName) {
            Map<String, Class> views = supportedAttributeViewNames
            if (views.containsKey(viewName)) {
                setCompressedAttribute(
                    (AbstractCompressorFilePath) path,
                    attribute,
                    value,
                    views[viewName]
                )
                return
            }
        }
        Path serviceLayer = ((AbstractCompressorFilePath) path).serviceLayerPath
        serviceLayer.fileSystem.provider().setAttribute(serviceLayer, attribute, value, options)
    }

    /**
     *
     * @param scheme Scheme that applies to the provider
     * @param filesystemClass Filesystem implementation class for the specific provider
     * @param pathClass Path implementation class for the specific provider
     *
     * @since 0.1
     */
    protected AbstractCompressorFileSystemProvider(
        final String scheme,
        Class<? extends AbstractCompressorFileSystem> filesystemClass,
        Class<? extends AbstractCompressorFilePath> pathClass
    ) {
        super(scheme)
        this.filesystemClass = filesystemClass
        this.pathClass = pathClass
    }

    /** Creates the compressed filesystem by invoking a new instance of the filesystem class that is specific
     * to the scheme that was specified during construction.
     *
     * @param uri URI for filesystem.
     * @param fileStoreProvider A way of accessing the filestroes for the secondary filesystem.
     * @param opts Options as typically supplied to {@link Files#newFileSystem(URI, Map < String, ? > )}
     * @return Created filesystem
     */
    @Override
    protected AbstractCompressorFileSystem doCreateCompressedFileSystem(
        URI uri,
        FileStoreProvider fileStoreProvider,
        Map<String, ?> opts
    ) {
        (AbstractCompressorFileSystem) (filesystemClass.newInstance(this, fileStoreProvider, opts))
    }

    /** Creates a path on a compressed filesystem by invoking a new instance of the path class that is specific
     * to the scheme that was specified during construction.
     *
     * @param fs Filesystem that path will be associated to.
     * @param servicePath Secondary kayer path that will serve the file in compressed format.
     * @return Created path
     *
     * @since 0.1
     */
    protected AbstractCompressedPath doCreateCompressedPath(AbstractCompressedFileSystem fs, Path servicePath) {
        (AbstractCompressedPath) (pathClass.newInstance(fs, servicePath))
    }

    /** Checks if read access to the file server by the secondary layer is available.
     *
     * @param path Path to check - Must extend {@link AbstractCompressorFilePath}
     * @return
     */
    @Override
    protected boolean hasReadAccess(AbstractPath path) {
        Files.isReadable(((AbstractCompressorFilePath) path).serviceLayerPath)
    }

    /** Checks if two compressed paths point to the same file on the secondary layer.
     *
     * @param path1 First compressed path
     * @param path2 Second compressed path
     * @return {@code true} if the paths point to same file.
     */
    @Override
    protected boolean doCheckIfSameFile(AbstractPath path1, AbstractPath path2) {
        Path servicePath1 = ((AbstractCompressorFilePath) path1).serviceLayerPath
        Path servicePath2 = ((AbstractCompressorFilePath) path2).serviceLayerPath

        if (servicePath1.fileSystem != servicePath2.fileSystem) {
            false
        } else {
            Files.isSameFile(servicePath1, servicePath2)
        }
    }

    /** The set of attribute view classes directly supported by this compressed file system.
     *
     * @return Set of attribute views. Can be empty, but never {@code null}.
     *
     * @since 0.1
     */
    abstract protected Set<Class> getSupportedAttributeViews()

    /** Returns the supported views in a map keyed by view name.
     *
     * @return Map of view name + implementing class name
     *
     * @since 0.1
     */
    abstract protected Map<String, Class> getSupportedAttributeViewNames()

    /** Creates a file attribute view on a comrepssed file system.
     *
     * @param path Compressed file path instance.
     * @param type Attribute view type that is provided for a compressed filesystem.
     * @param options Symbolic link options.
     * @return Instance of attribute view type.
     *
     * @throws UnsupportedOperationException if the view is not implemented.
     *
     * @since 0.1
     */
    protected <V extends FileAttributeView> V createFileAttributeView(AbstractCompressorFilePath path, Class<V> type, LinkOption... options) {
        throw new UnsupportedOperationException("No implementation defined to create an attribute view of type '${type.name}' for a path type of '${path.class.name}'")
    }

    /** Sets an attribute on a compressed filesystem
     *
     * If the comrpessed filesystem supports custom attribute views, this methid must be overridden to action those
     * views as the default implementation will simply throw an exception.
     *
     * @param path Compressed file path
     * @param attributeName Name of attribute for the comrpessed system that related to the attribute view class.
     * @param value New value of attribute.
     * @param viewClass Implementing clas for the attribute view.
     *
     * @throws UnsupportedOperationException if the view is not implemented.
     *
     * @since 0.1
     */
    protected void setCompressedAttribute(AbstractCompressorFilePath path,
                                          final String attributeName, Object value, final Class viewClass) {
        throw new UnsupportedOperationException("No implementation for setting ${attributeName} on attribute view type '${viewClass.name}'")
    }

    Set<OpenOption> extractLinkOptions(final Set<OpenOption> options) {
        options.findAll { opt ->
            opt instanceof LinkOption
        } as Set<OpenOption>
    }

    private Class filesystemClass
    private Class pathClass
}
