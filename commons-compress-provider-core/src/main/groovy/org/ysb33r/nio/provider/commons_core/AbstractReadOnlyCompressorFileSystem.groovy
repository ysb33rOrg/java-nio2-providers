/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.commons_core

import groovy.transform.CompileStatic
import org.ysb33r.nio.provider.core.FileStoreProvider

import java.nio.file.OpenOption

/** A compressor filesystem that can only supports read implementation by design.
 *
 * @since 0.1
 */
@CompileStatic
abstract class AbstractReadOnlyCompressorFileSystem extends AbstractCompressorFileSystem {
    protected AbstractReadOnlyCompressorFileSystem(AbstractCompressorFileSystemProvider provider, FileStoreProvider fileStoreProvider, Map<String, ?> env) {
        super(provider, fileStoreProvider, env)
    }

    /** Some compressed only filesystems allow read operations as te underlying library do not have support for write operations
     */
    @Override
    boolean isReadOnly() {
        true
    }

    /**
     *  @throws UnsupportedOperationException
     */
    @Override
    CompressingWriter getCompressingWriter(AbstractCompressorFilePath path, Set<? extends OpenOption> options) {
        throw new UnsupportedOperationException("Filesystem '${provider().getScheme()}' is read-only")

    }

    /**
     * @throws UnsupportedOperationException
     */
    @Override
    protected OutputStream createOutputStreamOver(OutputStream wrapped, Iterable<OpenOption> options) throws IOException {
        throw new UnsupportedOperationException("Filesystem '${provider().getScheme()}' is read-only")
    }
}
