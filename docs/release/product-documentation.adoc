= Java NIO2 Filesystem Providers
:imagesdir: images
:icons: font

This project provides a collection of NIO2 file providers. It aims to provide as rovidewrs as possible until some others can come up with (even) better code.

////
.Guidelines
* Getting started implementing a new filesystem
////

.Core libraries
[cols="70,20,10"]
|===
include::{gendocdir}/libs.adoc[]
|===

.Providers
[cols="70,20,10"]
|===
include::{gendocdir}/providers.adoc[]
|===

