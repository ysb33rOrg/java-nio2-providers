import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import internal.FileBasedProviderDetailsExtension
import internal.ProviderTemplateExtension
import org.apache.tools.ant.filters.ReplaceTokens
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.Copy
import org.gradle.api.tasks.TaskContainer

import static Nio2Constants.PROVIDER_TEMPLATE_EXT_NAME
import static Nio2Constants.SCHEME_DETAILS_EXT_NAME

/** This project is based upon Apache Commons Compress and most of the source code is generated from template
 *
 * @since 0.1
 */
@CompileStatic
class ApacheCommonsCompressFileBasedProviderGenerativePlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {

        ProviderTemplateExtension providerTemplate
        project.with {
            apply plugin: ApacheCommonsCompressFileBasedProviderPlugin

            providerTemplate = extensions.create(PROVIDER_TEMPLATE_EXT_NAME, ProviderTemplateExtension, project)
            providerTemplate.location "${project.projectDir.parentFile}/provider-template"
        }

        FileBasedProviderDetailsExtension scheme = (FileBasedProviderDetailsExtension) (project.extensions.getByName(SCHEME_DETAILS_EXT_NAME))
        Copy generatedSrc = addGeneratorTask(project, providerTemplate, scheme)
        addGeneratedLocationToSourceSets(project, providerTemplate)
        addTaskDependencies(project.tasks, generatedSrc)
    }

    Copy addGeneratorTask(
        Project p,
        ProviderTemplateExtension providerTemplate,
        FileBasedProviderDetailsExtension scheme
    ) {
        Copy copy = p.tasks.create(Nio2Constants.SRC_GENERATOR_TASK_NAME, Copy)
        Closure mainResources = { "${providerTemplate.location}/src/main/resources" }
        Closure mainGroovyR = { "${providerTemplate.location}/src/main/groovy/${scheme.readOnly ? 'ro' : 'rw'}" }
        Closure testGroovyR = { "${providerTemplate.location}/src/test/groovy/${scheme.readOnly ? 'ro' : 'rw'}" }
        Closure mainGroovyAll= { "${providerTemplate.location}/src/main/groovy/all" }
        Closure testGroovyAll = { "${providerTemplate.location}/src/test/groovy/all" }

        Closure asciidocTemplate = { "${providerTemplate.location}/src/docs/asciidoc" }
        String asciidocProject = 'src/docs/asciidoc'

        copy.into { providerTemplate.generatedSrcDir }
        copy.from mainResources, addEverytingTo('main/resources/META-INF/services')
        copy.from asciidocTemplate, addIndexDoc('docs/asciidoc')

        copy.from mainGroovyR, processTemplates('main/groovy/org/ysb33r/nio/provider', scheme)
        copy.from mainGroovyAll, processTemplates('main/groovy/org/ysb33r/nio/provider', scheme)
        copy.from testGroovyR, processTemplates('test/groovy/org/ysb33r/nio/provider', scheme)
        copy.from testGroovyAll, processTemplates('test/groovy/org/ysb33r/nio/provider', scheme)

        copy.from asciidocProject, addSchemeDocs('docs/asciidoc', scheme)
        copy.filter ReplaceTokens, beginToken: '@@',
            endToken: '@@',
            tokens: [
                scheme       : scheme.id,
                schemeFileExt: scheme.fileext,
                schemeName   : scheme.name,
                capScheme    : scheme.id.capitalize(),
                schemeSince  : scheme.since
            ]

        copy
    }

    void addTaskDependencies(TaskContainer tasks, Copy generateSource) {
        ['compileGroovy',
         'compileJava',
         'compileTestJava',
         'compileTestGroovy',
         'processResources',
         'processTestResources',
         'asciidoctor'
        ].each { String task ->
            tasks.getByName(task).dependsOn generateSource
        }
    }

    @CompileDynamic
    void addGeneratedLocationToSourceSets(Project project, ProviderTemplateExtension providerTemplate) {
        project.sourceSets {
            main {
                groovy {
                    srcDir "${providerTemplate.generatedSrcDir}/main/groovy"
                }
                resources {
                    srcDir "${providerTemplate.generatedSrcDir}/main/resources"
                }
            }
            test {
                groovy {
                    srcDir "${providerTemplate.generatedSrcDir}/test/groovy"
                }
            }
        }

        project.asciidoctor {
            sourceDir "${providerTemplate.generatedSrcDir}/docs/asciidoc"
        }
    }

    @CompileDynamic
    private Closure addEverytingTo(final String generationDestination) {
        return {
            include '*'
            into generationDestination
        }
    }

    @CompileDynamic
    private Closure processTemplates(
        final String generationDestination,
        FileBasedProviderDetailsExtension scheme
    ) {
        return {
            include '**/*.groovy.template'
            into "${generationDestination}/${scheme.id}"
            rename ~/(.+)\.template/, "${scheme.id.capitalize()}\$1"
        }
    }

    @CompileDynamic
    private Closure addIndexDoc(final String generationDestination) {
        return {
            include 'index.adoc'
            into generationDestination
        }
    }

    @CompileDynamic
    private Closure addSchemeDocs(
        final String generationDestination,
        FileBasedProviderDetailsExtension scheme
    ) {
        return {
            include "${scheme.id}.adoc"
            include "openoptions.csv"
            into generationDestination
        }
    }
}
