package internal

import groovy.transform.CompileStatic

/** An extension to describe a file-based provider.
 *
 * @since 0.1
 */
@CompileStatic
class ProviderDetailsExtension {

    /** Identifier. SHould match the default sheme in code.
     *
     */
    String id

    /** Name of provider. This is usually all captitals.
     *
     */
    String name

    /** Version this provider was first introduced.
     *
     */
    String since

    /** Whether the provider is a readonly filesystem
     *
     */
    boolean readOnly = false

    void id(String s) { this.id = s }

    void name(String s) { this.name = s }

    void since(String s) { this.since = s }

    void readOnly(boolean b) {this.readOnly = b}
}
