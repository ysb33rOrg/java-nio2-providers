package internal

import groovy.transform.CompileStatic

/** An extension to describe a file-based provider.
 *
 * @since 0.1
 */
@CompileStatic
class FileBasedProviderDetailsExtension extends ProviderDetailsExtension {

    /** Default file extension used for files.
     *
     */
    String fileext

    void fileext(String s) { this.fileext = s }
}
