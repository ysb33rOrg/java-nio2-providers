package internal

import groovy.transform.CompileStatic

/**
 * @since 0.1
 */
@CompileStatic
class Nio2PublishExtension {

    String bintrayRepoOwner = 'ysb33rorg'
    String bintrayRepoName  = 'nio2'
    String vcsUrl = 'https://gitlab.com/ysb33rOrg/java-nio2-providers.git'

    String license = 'Apache 2.0'

    void license(final String s) {
        this.license = s
    }

    String getDescription() {
        switch(this.description) {
            case Closure:
                return ((Closure)(this.description)).call()
            default:
                description.toString()
        }
    }

    void description(final Object s) {
        this.description = s
    }

    private Object description


}
