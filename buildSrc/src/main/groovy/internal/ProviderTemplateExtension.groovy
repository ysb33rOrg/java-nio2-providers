package internal

import groovy.transform.CompileStatic
import org.gradle.api.Project

/**
 * @since 0.1
 */
@CompileStatic
class ProviderTemplateExtension {

    ProviderTemplateExtension(Project p) {
        this.project = p
        this.generatedSrcDir = {"${project.buildDir}/generatedSrc"}
    }

    File getLocation() {
        project.file(this.location)
    }

    void location(Object loc) {
        this.location = loc
    }

    File getGeneratedSrcDir() {
        project.file(this.generatedSrcDir)
    }

    void generatedSrcDir(Object f) {
        this.generatedSrcDir = f
    }


    private final Project project
    private Object location
    private Object generatedSrcDir
}
