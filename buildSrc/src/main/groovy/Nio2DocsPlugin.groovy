import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project

import static Nio2Constants.getJAVA_COMPATIBILITY_VERSION

/**
 * @since 0.1
 */
@CompileStatic
class Nio2DocsPlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        project.with {
            apply plugin : 'base'
            apply plugin : 'org.asciidoctor.convert'
            apply plugin : 'org.ajoberstar.git-publish'

            extensions.extraProperties.set 'notSnapshot', { !((String) (project.version)).endsWith('-SNAPSHOT') }
            extensions.extraProperties.set 'websiteDir', file("${buildDir}/website/public")
            extensions.extraProperties.set 'genDocDir', file("${buildDir}/generated-docs")
        }

    }
}
