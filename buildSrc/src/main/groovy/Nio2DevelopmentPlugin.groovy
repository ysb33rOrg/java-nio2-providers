import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import internal.Nio2PublishExtension
import org.gradle.api.JavaVersion
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.bundling.Jar
import org.gradle.api.tasks.javadoc.Groovydoc

import static Nio2Constants.JAVA_COMPATIBILITY_VERSION
import static Nio2Constants.PROVIDER_PUBLISH_EXT_NAME

/**
 * @since 0.1
 */
@CompileStatic
class Nio2DevelopmentPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {


        Nio2PublishExtension providerPublishing

        project.with {
            apply plugin : Nio2DevelopmentBasePlugin

            apply plugin: 'org.asciidoctor.convert'

            apply plugin: 'maven'
            apply plugin: 'org.ysb33r.bintray'
            apply plugin: 'org.jetbrains.kotlin.jvm'

            providerPublishing = extensions.create(PROVIDER_PUBLISH_EXT_NAME, Nio2PublishExtension)
        }

        configureAdditionalJars(project)
        configureGroovyDoc(project)
        configureAsciidoctor(project)
        configureBintray(project, providerPublishing)
    }

    void configureGroovyDoc(Project p) {
        Groovydoc groovydoc = (Groovydoc) (p.tasks.getByName('groovydoc'))

        groovydoc.link(
            "https://docs.oracle.com/javase/${JAVA_COMPATIBILITY_VERSION.majorVersion}/docs/api/index.html",
            'java.nio.channels',
            'java.nio.channels.spi',
            'java.nio.charset',
            'java.nio.charset.spi',
            'java.nio.file',
            'java.nio.file.attribute',
            'java.nio.file.spi'
        )
    }

    @CompileDynamic
    void configureAsciidoctor(Project p) {

        p.repositories {
            maven {
                url 'https://repo.spring.io/release'
            }
        }

        p.dependencies {
            asciidoctor "io.spring.asciidoctor:spring-asciidoctor-extensions:${p.ext.versions.'asciidoc-spring-ext'}"
        }

        p.tasks.getByName('asciidoctor').configure {

            requires 'asciidoctor-diagram'

            inputs.files 'src/main'
            inputs.files 'src/test'

            separateOutputDirs = false

            sources {
                include 'index.adoc'
            }

            attributes 'source-highlighter': 'coderay',
                icons: 'font',
                toc: 'right',
                release: p.version,
                group: p.group,
                artifact: p.name,
                'java-version': JAVA_COMPATIBILITY_VERSION,
                'java-docs' : "https://docs.oracle.com/javase/${JAVA_COMPATIBILITY_VERSION.majorVersion}/docs/api/",
                'java-nio2-file' : "https://docs.oracle.com/javase/${JAVA_COMPATIBILITY_VERSION.majorVersion}/docs/api/java/nio/file/",
                'projdir' : "${p.projectDir.absolutePath}/"
        }
    }

    @CompileDynamic
    void configureAdditionalJars(Project p) {

        p.with {
            tasks.create('sourcesJar', Jar).configure {
                dependsOn 'classes'
                classifier = 'sources'
                from sourceSets.main.allSource
            }

            p.tasks.create('javadocJar', Jar).configure {
                description "An archive of the JavaDocs for Maven Central"
                classifier 'javadoc'
                from javadoc
                from groovydoc
            }

            artifacts {
                archives sourcesJar, javadocJar
            }
        }
    }

    @CompileDynamic
    void configureBintray(Project p, Nio2PublishExtension providerPublishing) {
        p.with {
            tasks.getByName('uploadArchives').configure {
                repositories {
                    bintrayMavenDeployer {
                        username project.properties.bintrayUserName
                        apiKey project.properties.bintrayApiKey
                        repoOwner { providerPublishing.bintrayRepoOwner }
                        repoName { providerPublishing.bintrayRepoName }
                        packageName project.name
                        description { providerPublishing.description }
                        tags 'java', 'groovy', 'nio2', project.projectDir.name
                        licenses { providerPublishing.license }
                        vcsUrl { providerPublishing.vcsUrl }
                        autoCreatePackage true
                        updatePackage true
                        versionAttributes 'nio2-provider': "${project.projectDir.parentFile.name}:${project.projectDir.name}:${project.version}"
                    }
                }

                mustRunAfter check
                onlyIf notSnapshot
            }

            tasks.getByName('bintrayMetadata_uploadArchives').dependsOn assemble
        }
    }
}


