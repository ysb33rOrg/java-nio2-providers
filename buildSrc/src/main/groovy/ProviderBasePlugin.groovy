import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * @since
 */
@CompileStatic
class ProviderBasePlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        project.with {
            apply plugin : Nio2DevelopmentPlugin
        }
    }
}
