import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import internal.FileBasedProviderDetailsExtension
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.javadoc.Groovydoc
import org.gradle.api.tasks.testing.Test

/** This project is based upon Apache Commons Compress.
 *
 * @since 0.1
 */
@CompileStatic
class ApacheCommonsCompressFileBasedProviderPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        project.with {
            apply plugin: FileBasedProviderPlugin
            apply plugin : 'groovy'
        }

        configureTestTask(project)
        configureGroovydoc(project)
    }

    void configureGroovydoc(Project p) {

        Groovydoc groovydoc = (Groovydoc) (p.tasks.getByName('groovydoc'))
        Map versions = (Map)(p.extensions.extraProperties.get('versions'))
        groovydoc.link(
            "https://commons.apache.org/proper/commons-compress/javadocs/api-${versions.commonscompress}/index.html",
                'org.apache.commons.compress'
        )

    }
    @CompileDynamic
    void configureTestTask(Project project) {
        // Set these two values on compressor provider tests, due to the global nature of filesystems.
        project.tasks.getByName('test').configure {
            forkEvery 1
            maxParallelForks 2
        }
    }
}
