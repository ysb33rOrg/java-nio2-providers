import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project

import static Nio2Constants.JAVA_COMPATIBILITY_VERSION

/**
 * @since 0.1
 */
@CompileStatic
class Nio2DevelopmentBasePlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        project.with {
            apply plugin: 'java'
            apply plugin: 'groovy'
            extensions.extraProperties.set 'notSnapshot', { !((String) (project.version)).endsWith('-SNAPSHOT') }
        }

        setJdkVersion(project)
    }

    @CompileDynamic
    void setJdkVersion(Project p) {
        p.with {
            sourceCompatibility = JAVA_COMPATIBILITY_VERSION
            targetCompatibility = JAVA_COMPATIBILITY_VERSION
        }
    }
}
