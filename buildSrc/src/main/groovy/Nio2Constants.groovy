import groovy.transform.CompileStatic
import org.gradle.api.JavaVersion

import static org.gradle.api.JavaVersion.VERSION_1_8

/**
 * @since
 */
@CompileStatic
class Nio2Constants {

    // Version of Java this project has to be compatible with
    static final JavaVersion JAVA_COMPATIBILITY_VERSION = VERSION_1_8


    // Extensions
    final static String SCHEME_DETAILS_EXT_NAME = 'scheme'
    final static String PROVIDER_TEMPLATE_EXT_NAME = 'providerTemplate'
    final static String PROVIDER_PUBLISH_EXT_NAME = 'providerPublishing'

    // Tasks
    final static String SRC_GENERATOR_TASK_NAME = 'generateSource'

    // Projects
//    final static String APACHE_COMMONS_COMPRESS_CORE_LIB = ':nio-commons-compress-provider-core'
}
