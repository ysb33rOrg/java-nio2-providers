import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import internal.FileBasedProviderDetailsExtension
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task

/**
 * @since 0.1
 */
@CompileStatic
class FileBasedProviderPlugin implements Plugin<Project> {

    final static String SCHEME_EXT_NAME = Nio2Constants.SCHEME_DETAILS_EXT_NAME

    @Override
    void apply(Project project) {

        FileBasedProviderDetailsExtension scheme

        project.with {
            apply plugin: ProviderBasePlugin
            scheme = extensions.create(SCHEME_EXT_NAME, FileBasedProviderDetailsExtension)
            apply from: "${projectDir}/scheme.gradle"
        }

        configureAsciidoctor(project,scheme)
    }

    @CompileDynamic
    void configureAsciidoctor(Project project, FileBasedProviderDetailsExtension scheme ) {
        project.tasks.getByName('asciidoctor').configure {
            attributes scheme: scheme.id
            attributes schemefileext: scheme.fileext
            attributes schemename: scheme.name

            if(scheme.readOnly) {
                attributes schemereadonly: '1'
            }
        }
    }
}
