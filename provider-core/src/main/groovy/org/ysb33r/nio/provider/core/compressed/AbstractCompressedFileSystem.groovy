/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.core.compressed

import groovy.transform.CompileStatic
import org.ysb33r.nio.provider.core.AbstractFileSystem
import org.ysb33r.nio.provider.core.FileStoreProvider

import java.nio.file.FileStore
import java.nio.file.InvalidPathException
import java.nio.file.OpenOption
import java.nio.file.Path
import java.nio.file.spi.FileSystemProvider

/** Base class for filesystems serving compressed content up as uncompressed.
 *
 * @since 0.1
 */
@CompileStatic
abstract class AbstractCompressedFileSystem extends AbstractFileSystem {

    /** Compressed filesystems are not navigable, and paths cannot be
     * constructed once the filesystem has been bound to the secondary layer.

     * <p> Therefore this method always throws {@link java.nio.file.InvalidPathException}.
     *
     * A future implementation might try to resolve siblings on the secondary filesystem.
     *
     * @throws InvalidPathException
     *
     */
    @Override
    Path getPath(String first, String... more) {
        throw new InvalidPathException("${first},${more.join(',')}","Cannot infer a new path on the filesystem once it has been constructed.")
    }

    /**
     * Returns an object to iterate over the underlying file stores.
     *
     * A compressed filesystem will only return one FileStore, being the one on which the URI
     * was used during construction of the filesystem
     *
     * <p> The elements of the returned iterator are the {@link
     * java.nio.file.FileStore FileStores} for this file system. The order of the elements is
     * not defined and the file stores may change during the lifetime of the
     * Java virtual machine. When an I/O error occurs, perhaps because a file
     * store is not accessible, then it is not returned by the iterator.
     *
     * <p> In the case of the default provider, and a security manager is
     * installed, the security manager is invoked to check {@link
     * RuntimePermission}<tt>("getFileStoreAttributes")</tt>. If denied, then
     * no file stores are returned by the iterator. In addition, the security
     * manager's {@link SecurityManager#checkRead(String)} method is invoked to
     * check read access to the file store's <em>top-most</em> directory. If
     * denied, the file store is not returned by the iterator. It is system
     * dependent if the permission checks are done when the iterator is obtained
     * or during iteration.
     *
     * <p> <b>Usage Example:</b>
     * Suppose we want to print the space usage for all file stores:
     * <pre>
     *     for (FileStore store: FileSystems.getDefault().getFileStores()) {*         long total = store.getTotalSpace() / 1024;
     *         long used = (store.getTotalSpace() - store.getUnallocatedSpace()) / 1024;
     *         long avail = store.getUsableSpace() / 1024;
     *         System.out.format("%-20s %12d %12d %12d%n", store, total, used, avail);
     *}* </pre>
     *
     * @return An object to iterate over the backing file stores.
     */
    @Override
    Iterable<FileStore> getFileStores() {
        this.fileStores.getFileStores()
    }

    /** Exracts options that are specific to this filesystem type.
     *
     * @return Redacted set of options.
     */
    abstract Set<OpenOption> getCompressionSpecificOptions(Iterable<OpenOption> options)

    /** Intermediary constructor that will set defaults for the super class.
     *
     * <p> It specifically signals that compressed file systems do not have the concept of
     * subpaths (unlike archive file systems).
     *
     * @param provider A provider that extends {@link AbstractCompressedFileSystemProvider}.
     *
     * @since 0.1
     */
    protected AbstractCompressedFileSystem(AbstractCompressedFileSystemProvider provider, FileStoreProvider fileStoreProvider) {
        super(provider, null)
        this.fileStores = fileStoreProvider
    }

    private FileStoreProvider fileStores
}
