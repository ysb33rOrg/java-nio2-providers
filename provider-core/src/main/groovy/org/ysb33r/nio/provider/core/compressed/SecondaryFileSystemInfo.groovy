/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.core.compressed

import java.nio.file.spi.FileSystemProvider

/** Provides information on a secondary filesystem.
 *
 * @since 0.1
 */
interface SecondaryFileSystemInfo {

    /** The provider of a secondary filesystem
     *
     * @return Filesystem provider
     */
    FileSystemProvider getSecondaryProvider()

    /** The URI representing a path on a secondary filesystem
     *
     * @return URI of secondary filesystem
     */
    URI getSecondaryURI()

    /** The URI representing a root URI on a secondary filesystem
     *
     * @return Root URI of secondary filesystem
     */
    URI getSecondaryRootURI()
}