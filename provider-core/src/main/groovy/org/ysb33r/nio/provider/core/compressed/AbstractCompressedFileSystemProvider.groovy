/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.core.compressed

import groovy.transform.CompileStatic
import groovy.transform.Synchronized
import org.ysb33r.nio.provider.core.AbstractFileSystemProvider
import org.ysb33r.nio.provider.core.FileStoreProvider
import org.ysb33r.nio.provider.core.UriUtils

import java.nio.file.AccessDeniedException
import java.nio.file.AccessMode
import java.nio.file.DirectoryStream
import java.nio.file.FileStore
import java.nio.file.FileSystem
import java.nio.file.FileSystemAlreadyExistsException
import java.nio.file.FileSystemNotFoundException
import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.LinkOption
import java.nio.file.NoSuchFileException
import java.nio.file.NotDirectoryException
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.attribute.BasicFileAttributes

/** Base class for providers that serve compressed files in uncompressed format.
 *
 * @since 0.1
 */
@CompileStatic
abstract class AbstractCompressedFileSystemProvider extends AbstractFileSystemProvider {

    /** Compressed filesystems do not support directories.
     *
     * Always throws an exception.
     *
     * @throws java.nio.file.NotDirectoryException
     */
    @Override
    DirectoryStream<Path> newDirectoryStream(Path dir, DirectoryStream.Filter<? super Path> filter) throws IOException {
        throw new NotDirectoryException("Filesystems with scheme '${getScheme()}' do not support directories")
    }

    /**
     * Constructs a new {@code FileSystem} object identified by a URI. This
     * method is invoked by the {@link java.nio.file.FileSystems#newFileSystem(URI, Map)}
     * method to open a new file system identified by a URI.
     *
     * <p> The {@code uri} parameter is an absolute, hierarchical URI, with a
     * scheme equal (without regard to case) to the scheme supported by this
     * provider. The exact form of the URI is highly provider dependent. The
     * {@code env} parameter is a map of provider specific properties to configure
     * the file system.
     *
     * <p> Compressed filesystem providers utilise a secondary filesystem to serve up the compressed
     * data. If the secondary filesystem does not exist, an attempt will be made to create it passing
     * the same options that were passed to this method. If it does exist it be just be used.
     *
     * <p> This method will throw {@link FileSystemAlreadyExistsException} if the
     * secondary file system is already attached to the specific compressed filesystem indentified by the scheme.
     *
     * <p> If the filesystem has been closed it can be re-created by a call to this method.
     *
     * @param uri
     *          URI reference
     * @param env
     *          A map of provider specific properties to configure the file system;
     *          may be empty
     *
     * @return A new file system
     *
     * @throws IllegalArgumentException
     *          If the pre-conditions for the {@code uri} parameter aren't met,
     *          or the {@code env} parameter does not contain properties required
     *          by the provider, or a property value is invalid
     * @throws IOException
     *          An I/O error occurs creating the file system
     * @throws SecurityException
     *          If a security manager is installed and it denies an unspecified
     *          permission required by the file system provider implementation
     * @throws FileSystemAlreadyExistsException
     *          If the file system has already been created
     */
    @Override
    FileSystem newFileSystem(URI uri, Map<String, ?> env) throws IOException {
        if(uri.scheme.toLowerCase() != getScheme().toLowerCase()) {
            throw new IllegalArgumentException("'${uri}' is not a valid. Requires scheme '${getScheme()}'")
        }

        SecondaryFileSystemInfo secondary = FileSystemLayerParser.extractSecondaryFileSystem(uri)
        FileSystem secondaryFS

        try {
            secondaryFS = secondary.secondaryProvider.getFileSystem(secondary.secondaryRootURI)
        } catch (FileSystemNotFoundException) {
            secondaryFS = secondary.secondaryProvider.newFileSystem(secondary.secondaryRootURI,env)
        }

        return createAndRegisterFileSystem(secondaryFS,uri,env)
    }

    /**
     * Returns an existing {@code FileSystem} created by this provider.
     *
     * <p> This method returns a reference to a {@code FileSystem} that was
     * created by invoking the {@link #newFileSystem(URI, Map) newFileSystem(URI,Map)}
     * method. File systems created the {@link #newFileSystem(Path, Map)
     * newFileSystem(Path,Map)} method are not returned by this method.
     * The file system is identified by its {@code URI}. Its exact form
     * is highly provider dependent. In the case of the default provider the URI's
     * path component is {@code "/"} and the authority, query and fragment components
     * are undefined (Undefined components are represented by {@code null}).
     *
     * <p> If the filesystem is already clsoed, it will still be returned.
     * It is up to the caller to create a new instance of the filesystem via a call
     * to {@link #newFileSystem} in that case.
     * @param uri
     *          URI reference
     *
     * @return The file system
     *
     * @throws IllegalArgumentException
     *          If the pre-conditions for the {@code uri} parameter aren't met
     * @throws FileSystemNotFoundException
     *          If the file system does not exist
     * @throws SecurityException
     *          If a security manager is installed and it denies an unspecified
     *          permission.
     */
    @Override
    FileSystem getFileSystem(URI uri) {
        if(uri.scheme.toLowerCase() != getScheme().toLowerCase()) {
            throw new IllegalArgumentException("'${uri}' is not a valid. Requires scheme '${getScheme()}'")
        }

        SecondaryFileSystemInfo secondary = FileSystemLayerParser.extractSecondaryFileSystem(uri)
        URI secondaryURI = secondary.secondaryURI
        FileSystem secondaryFS = secondary.secondaryProvider.getPath(secondaryURI).fileSystem
        registry.get(secondaryFS)
    }


    /**
     * Return a {@code Path} object by converting the given {@link URI}. The
     * resulting {@code Path} is associated with a {@link java.io.FileSystem} that
     * already exists or is constructed automatically.
     *
     * <p> The exact form of the URI is file system provider dependent. In the
     * case of the default provider, the URI scheme is {@code "file"} and the
     * given URI has a non-empty path component, and undefined query, and
     * fragment components. The resulting {@code Path} is associated with the
     * default {@link FileSystems#getDefault default} {@code FileSystem}.
     *
     * <p> If a security manager is installed then a provider implementation
     * may require to check a permission. In the case of the {@link
     * FileSystems # getDefault default} file system, no permission check is
     * required.
     *
     * @param uri
     *          The URI to convert
     *
     * @return The resulting {@code Path}
     *
     * @throws IllegalArgumentException
     *          If the URI scheme does not identify this provider or other
     *          preconditions on the uri parameter do not hold
     * @throws FileSystemNotFoundException
     *          The file system, identified by the URI, does not exist and
     *          cannot be created automatically
     * @throws SecurityException
     *          If a security manager is installed and it denies an unspecified
     *          permission.
     */
    @Override
    Path getPath(URI uri) {
        FileSystem fs
        try {
            fs = getFileSystem(uri)
        } catch(FileSystemNotFoundException) {
            fs = newFileSystem(uri,[:])
        }

        doCreateCompressedPath((AbstractCompressedFileSystem)fs,uri)
    }

    /**
     * Returns the {@link java.nio.file.FileStore} representing the file store where a file
     * is located on the secondary filesyste,. This method works in exactly the manner specified by the
     * {@link Files#getFileStore} method.
     *
     * @param path
     *          the path to the file
     *
     * @return the file store where the compressed file is stored
     *
     * @throws IOException
     *          if an I/O error occurs
     * @throws SecurityException
     *          In the case of the service path being on default provider, and a security manager is
     *          installed, the {@link SecurityManager#checkRead(String) checkRead}
     *          method is invoked to check read access to the file, and in
     *          addition it checks {@link RuntimePermission}<tt>
     *          ("getFileStoreAttributes")</tt>
     */
    @Override
    FileStore getFileStore(Path path) throws IOException {
        Path servicePath = ((AbstractCompressedPath)path).serviceLayerPath
        Files.getFileStore(servicePath)
    }

    /**
     * Tells whether or not a file is considered <em>hidden</em>. This method
     * works in exactly the manner specified by the {@link Files#isHidden}
     * method.
     *
     * <p> This method is invoked by the {@link Files#isHidden isHidden} method.
     *
     * @param path
     *          the path to the file to test
     *
     * @return {@code true} if the file is considered hidden
     *
     * @throws IOException
     *          if an I/O error occurs
     * @throws SecurityException
     *          In the case of the default provider, and a security manager is
     *          installed, the {@link SecurityManager#checkRead(String) checkRead}
     *          method is invoked to check read access to the file.
     */
    @Override
    boolean isHidden(Path path) throws IOException {
        Path servicePath = ((AbstractCompressedPath)path).serviceLayerPath
        Files.isHidden(servicePath)
    }



    /**
     * Reads a file's attributes as a bulk operation. This method works in
     * exactly the manner specified by the {@link
     * Files # readAttributes ( Path , Class , LinkOption[] )} method.
     *
     * @param path
     *          the path to the file
     * @param type
     *          the {@code Class} of the file attributes required
     *          to read
     * @param options
     *          options indicating how symbolic links are handled
     *
     * @return the file attributes
     *
     * @throws UnsupportedOperationException
     *          if an attributes of the given type are not supported
     * @throws IOException
     *          if an I/O error occurs
     * @throws SecurityException
     *          In the case of the default provider, a security manager is
     *          installed, its {@link SecurityManager#checkRead(String) checkRead}
     *          method is invoked to check read access to the file
     */
    @Override
    def <A extends BasicFileAttributes> A readAttributes(Path path, Class<A> type, LinkOption... options) throws IOException {
        Path servicePath = ((AbstractCompressedPath)path).serviceLayerPath
        servicePath.fileSystem.provider().readAttributes(servicePath,type,options)
    }


    /**
     * Checks the existence, and optionally the accessibility, of a file.
     *
     * <p> This method may be used by the {@link Files#isReadable isReadable},
     * {@link Files#isWritable isWritable} and {@link Files#isExecutable
     * isExecutable} methods to check the accessibility of a file.
     *
     * <p> This method checks the existence of a file and that this Java virtual
     * machine has appropriate privileges that would allow it access the file
     * according to all of access modes specified in the {@code modes} parameter
     * as follows:
     *
     * <table border=1 cellpadding=5 summary="">
     * <tr> <th>Value</th> <th>Description</th> </tr>
     * <tr>
     *   <td> {@link java.nio.file.AccessMode#READ READ} </td>
     *   <td> Checks that the file exists and that the Java virtual machine has
     *     permission to read the file. </td>
     * </tr>
     * <tr>
     *   <td> {@link java.nio.file.AccessMode#WRITE WRITE} </td>
     *   <td> Checks that the file exists and that the Java virtual machine has
     *     permission to write to the file, </td>
     * </tr>
     * <tr>
     *   <td> {@link java.nio.file.AccessMode#EXECUTE EXECUTE} </td>
     *   <td> Checks that the file exists and that the Java virtual machine has
     *     permission to {@link Runtime#exec execute} the file. The semantics
     *     may differ when checking access to a directory. For example, on UNIX
     *     systems, checking for {@code EXECUTE} access checks that the Java
     *     virtual machine has permission to search the directory in order to
     *     access file or subdirectories. </td>
     * </tr>
     * </table>
     *
     * <p> If the {@code modes} parameter is of length zero, then the existence
     * of the file is checked.
     *
     * <p> This method follows symbolic links if the file referenced by this
     * object is a symbolic link. Depending on the implementation, this method
     * may require to read file permissions, access control lists, or other
     * file attributes in order to check the effective access to the file. To
     * determine the effective access to a file may require access to several
     * attributes and so in some implementations this method may not be atomic
     * with respect to other file system operations.
     *
     * @param path
     *          the path to the file to check
     * @param modes
     *          The access modes to check; may have zero elements
     *
     * @throws UnsupportedOperationException
     *          an implementation is required to support checking for
     * {@code READ}, {@code WRITE}, and {@code EXECUTE} access. This
     *          exception is specified to allow for the {@code Access} enum to
     *          be extended in future releases.
     * @throws NoSuchFileException
     *          if a file does not exist <i>(optional specific exception)</i>
     * @throws AccessDeniedException
     *          the requested access would be denied or the access cannot be
     *          determined because the Java virtual machine has insufficient
     *          privileges or other reasons. <i>(optional specific exception)</i>
     * @throws IOException
     *          if an I/O error occurs
     * @throws SecurityException
     *          In the case of the default provider, and a security manager is
     *          installed, the {@link SecurityManager#checkRead(String) checkRead}
     *          is invoked when checking read access to the file or only the
     *          existence of the file, the {@link SecurityManager#checkWrite(String)
     *          checkWrite} is invoked when checking write access to the file,
     *          and {@link SecurityManager#checkExec(String) checkExec} is invoked
     *          when checking execute access.
     */
    @Override
    void checkAccess(Path path, AccessMode... modes) throws IOException {
        Path servicePath = ((AbstractCompressedPath)path).serviceLayerPath
        servicePath.fileSystem.provider().checkAccess(servicePath,modes)
    }

    /**
     * Reads a set of file attributes as a bulk operation. This method works in
     * exactly the manner specified by the {@link
     * Files # readAttributes ( Path , String , LinkOption[] )} method.
     *
     * @param path
     *          the path to the file
     * @param attributes
     *          the attributes to read
     * @param options
     *          options indicating how symbolic links are handled
     *
     * @return a map of the attributes returned; may be empty. The map's keys
     *          are the attribute names, its values are the attribute values
     *
     * @throws UnsupportedOperationException
     *          if the attribute view is not available
     * @throws IllegalArgumentException
     *          if no attributes are specified or an unrecognized attributes is
     *          specified
     * @throws IOException
     *          If an I/O error occurs
     * @throws SecurityException
     *          In the case of the default provider, and a security manager is
     *          installed, its {@link SecurityManager#checkRead(String) checkRead}
     *          method denies read access to the file. If this method is invoked
     *          to read security sensitive attributes then the security manager
     *          may be invoke to check for additional permissions.
     */
    @Override
    Map<String, Object> readAttributes(Path path, String attributes, LinkOption... options) throws IOException {
        Path servicePath = ((AbstractCompressedPath)path).serviceLayerPath
        servicePath.fileSystem.provider().readAttributes(servicePath,attributes,options)
    }

    /** Go-between contructor doing nothing more than passing the scheme name to the super class.
     *
     * @param scheme Scheme for this provider
     *
     * @since 0.1
     */
    protected AbstractCompressedFileSystemProvider(String scheme) {
        super(scheme)
    }

    /** Takes a URI and extracts a service path from it and creates a suitable compressed path
     * on the filesystem.
     *
     * @param fs Filesystem that will be associated with this path.
     * @param uri URI
     * @return Path
     *
     * @since 0.1
     */
    protected AbstractCompressedPath doCreateCompressedPath(AbstractCompressedFileSystem fs, final URI uri) {

        Path servicePath
        if(uri.opaque) {
            URI extractedUri = uri.rawSchemeSpecificPart.toURI()

            if(extractedUri.scheme) {
                servicePath = Paths.get(extractedUri)
            } else {
                servicePath = FileSystems.default.getPath(extractedUri.path)
            }

        } else {
            servicePath = FileSystems.default.getPath(uri.path)
        }
        doCreateCompressedPath(fs,servicePath)
    }

    /** Perform the actual work of creating the filesystem.
     *
     * @param uri URI that represented the filesystem.
     * @param fileStoreProvider A way of accessing the file stores for this filesystem.
     * @param opts Additional options for creating the filesystem.
     *
     * @throws FileSystemAlreadyExistsException If the file system has already been created.
     *
     * @since 0.1
    */
    abstract protected AbstractCompressedFileSystem doCreateCompressedFileSystem(
        URI uri,
        FileStoreProvider fileStoreProvider,
        Map<String,?> opts
    )

    /** Creates a compressed path by layering over another path from a filesystem that will server the data in
     * compressed form.
     *
     * @param fs Existing compressed filesystem to associate compressed path.
     * @param servicePath Layered path that will service the data in compressed form to this filesystem.
     * @return Compressed path instance.
     *
     * @since 0.1
     */
    abstract protected AbstractCompressedPath doCreateCompressedPath(AbstractCompressedFileSystem fs, Path servicePath)

    /** Access to the filesystem registry.
     *
     * <p> This call is only provided for implementers of a compressed filesystem that need to override the
     *   {@link #newFileSystem} and {@link @getFileSystem} methods that are provided.
     *
     * @return Filesystem registry for a compressed filesystem provider.
     *
     * @since 0.1
     */
    protected CompressedFileSystemRegistry getRegistry() {
        this.registry
    }

    @Synchronized
    private FileSystem createAndRegisterFileSystem(final FileSystem secondaryFS,final URI uri,final Map<String,?> env) {
        FileSystem newFS
        FileStoreProvider fileStoreProvider = { -> secondaryFS.fileStores } as FileStoreProvider
        if(registry.contains(secondaryFS)) {
            FileSystem fs2 = registry.get(secondaryFS)
            if(fs2.isOpen()) {
                throw new FileSystemAlreadyExistsException("Filesystem for ${UriUtils.friendlyURI(uri)} already exists")
            } else {
                newFS = doCreateCompressedFileSystem(uri,fileStoreProvider,env)
                registry.replace(secondaryFS,newFS)
            }
        } else {
            newFS = doCreateCompressedFileSystem(uri,fileStoreProvider,env)
            registry.add(secondaryFS,newFS)
        }
        return newFS
    }

    private CompressedFileSystemRegistry registry = new CompressedFileSystemRegistry()
}
