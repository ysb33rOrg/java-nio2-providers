/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.core.compressed

import groovy.transform.CompileStatic

import java.nio.file.LinkOption
import java.nio.file.Path
import java.nio.file.attribute.FileAttribute
import java.nio.file.attribute.FileAttributeView

/** Presents the ability to attributes from the lower layer as well as custom attributes from the
 * compressed system.
 *
 * @since 0.1
 */
@CompileStatic
abstract class AbstractCompressedFileAttributeView implements FileAttributeView {

    /** Returns a view of the secondary file / filesystem's file attributes.
     *
     * @return Access to the view of the secondary filesystem
     */
    public <T extends FileAttributeView> T getSecondaryAs(Class<T> clazz, LinkOption... options) {
        serviceLayerPath.fileSystem.provider().getFileAttributeView(serviceLayerPath, clazz, options)
    }

    /** Returns an attribute from the compressed filesystem converted to
     * it string representation.
     *
     * The default implementation will simply call {code toString()} on a
     * non-null attribute.
     *
     * @param name Name of the attribute.
     * @return Attribute is if exists, {@code null} if it is supported by the
     *   compression layer, but not available at the point of call.
     * @throw UnsupportedOperationException if a attribute is not supported.
     *
     * @since 0.1
     */
    String getProperty(String name) {
        getAttribute(name)?.toString()
    }

    /** Returns an attribute from the compressed filesystem.
     *
     * @param name Name of the attribute.
     * @return Attribute if it exists, {@code null} if it is supported by the
     *   compression layer, but not available at the point of call.
     * @throw UnsupportedOperationException if a attribute is not supported.
     *
     * @since 0.1
     */
    abstract FileAttribute getAttribute(String name)

    protected AbstractCompressedFileAttributeView(Path secondaryLayerPath) {
        this.serviceLayerPath = secondaryLayerPath
    }

    private final Path serviceLayerPath
}
