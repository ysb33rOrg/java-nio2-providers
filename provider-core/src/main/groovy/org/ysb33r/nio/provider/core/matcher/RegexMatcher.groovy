/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.core.matcher

import groovy.transform.CompileStatic

import java.nio.file.Path
import java.nio.file.PathMatcher
import java.util.regex.Pattern

/** Implements a Regex-based path matcher.
 *
 * @since 0.1
 */
@CompileStatic
class RegexMatcher implements PathMatcher {

    /** Create a Regex matcher from an string-based pattern.
     *
     * @param pat Pattern to match paths
     */
    RegexMatcher(final String pat) {
        this.regex = ~/${pat}/
    }

    /** Create a Regex matcher from an existing pattern.
     *
     * @param pat Pattern to match paths
     */
    RegexMatcher(final Pattern pat) {
        this.regex = pat
    }

    /**
     * Tells if given path matches this matcher's pattern.
     *
     * @param path
     *          the path to match
     *
     * @return {@code true} if, and only if, the path matches this
     *          matcher's pattern
     */
    @Override
    boolean matches(Path path) {
        path.toString() =~ regex
    }

    final Pattern regex
}
