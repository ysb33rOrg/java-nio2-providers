/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.core

import groovy.transform.CompileStatic

import java.nio.file.DirectoryStream
import java.nio.file.FileSystem
import java.nio.file.spi.FileSystemProvider

/** An abstract class that provides basic filesystem implementation functionality.
 *
 * @since 0.1
 */
@CompileStatic
abstract class AbstractFileSystem extends FileSystem {

    /**
     * Returns the provider that created this file system.
     *
     * @return The provider that created this file system.
     */
    @Override
    FileSystemProvider provider() {
        fileSystemProvider
    }

    /**
     * Tells whether or not this file system is open.
     *
     * @return {@code true} if, and only if, this file system is open
     */
    @Override
    boolean isOpen() {
        open
    }

    /**
     * Closes this file system.
     *
     * <p> After a file system is closed then all subsequent access to the file
     * system, either by methods defined by this class or on objects associated
     * with this file system, throw {@link java.nio.file.ClosedFileSystemException}. If the
     * file system is already closed then invoking this method has no effect.
     *
     * <p> Closing a file system will close all open {@link
     * java.nio.channels.Channel channels}, {@link DirectoryStream directory-streams},
     * {@link java.nio.file.WatchService watch-service}, and other closeable objects associated
     * with this file system.
     *
     * @throws IOException
     *          If an I/O error occurs
     * @throws UnsupportedOperationException
     *          Thrown in the case of the default file system
     */
    @Override
    // tag::close[]
    void close() {
        if (isOpen()) {
            doCloseChannels()
            doCloseDirectoryStreams()
            doCloseChannels()
            doCloseFileSystem()
            this.open = false
        }
    }
    // end::close[]

    /**
     * Returns the name separator, represented as a string.
     *
     * <p> The name separator is used to separate names in a path string. An
     * implementation may support multiple name separators in which case this
     * method returns an implementation specific <em>default</em> name separator.
     * This separator is used when creating path strings by invoking the {@link
     * java.nio.file.Path # toString ( ) toString()} method.
     *
     * @return The name separator
     */
    @Override
    String getSeparator() {
        return '/'
    }

    /** The token that is used to search backwards in a URI to find a local subpath
     *
     * @return The subpath separator or {@code null} is layering is not supported
     */
    String getLayerSubPathToken() {
        this.layerSubPathToken
    }

    /** Indicates whether this FileSystem can be used as an outer layer on another filesystem.
     *
     * @return {@code true} is this filesystem allows layering.
     */
    boolean allowsLayeredFileSystems() {
        this.layerSubPathToken != null
    }

    /** Constructs this instances by providing a link to provider that is instantiating it as well as a sub path token.
     *
     * @param provider Link to filesystem provider that is creating this filesystem instance.
     * @param subPathToken This is the character or sequence of characters that defines a subpath in a layered filesystem
     */
    protected AbstractFileSystem(FileSystemProvider provider, String subPathToken) {
        this.fileSystemProvider = provider
        this.layerSubPathToken = subPathToken
    }

    /**
     * Performs the physical actions of closing a filesystem.
     *
     * <p> Closing a file system will close all open {@link
     * java.nio.channels.Channel channels}, {@link DirectoryStream directory-streams},
     * {@link java.nio.file.WatchService watch-service}, and other closeable objects associated
     * with this file system.
     *
     * @throws IOException
     *          If an I/O error occurs
     *
     * @since 0.1
     */
    abstract protected void doCloseFileSystem()

    /** Performs the physical action of closing all channels associated with this filesystem
     *
     * @throws IOException
     *          If an I/O error occurs
     *
     * @since 0.1
     */
    abstract protected void doCloseChannels()

    /** Performs the physical action of closing all directory streams associated with this filesystem
     *
     * @throws IOException
     *          If an I/O error occurs
     *
     * @since 0.1
     */
    abstract protected void doCloseDirectoryStreams()

    /** Performs the physical action of closing all watch services associated with this filesystem
     *
     * @throws IOException
     *          If an I/O error occurs
     *
     * @since 0.1
     */
    abstract protected void doCloseWatchServices()

    private final FileSystemProvider fileSystemProvider
    private boolean open = true
    private String layerSubPathToken
}
