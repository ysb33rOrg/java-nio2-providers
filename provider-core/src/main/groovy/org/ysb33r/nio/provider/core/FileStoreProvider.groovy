/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.core

import groovy.transform.CompileStatic

import java.nio.file.FileStore

/** Functional interface that when called will return a list of {@link java.nio.file.FileStore} instances.
 *
 * @since 0.1
 */
@CompileStatic
interface FileStoreProvider {

    /** Get iterable collection of {@link java.nio.file.FileStore} instances.
     *
     * @return Iterable list of file stores. Can be empty, never {@code null}.
     */
    Iterable<FileStore> getFileStores()
}