/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.core.compressed

import groovy.transform.CompileStatic
import org.ysb33r.nio.provider.core.AbstractPath

import java.nio.file.FileSystem
import java.nio.file.Files
import java.nio.file.InvalidPathException
import java.nio.file.LinkOption
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.WatchEvent
import java.nio.file.WatchKey
import java.nio.file.WatchService
import java.nio.file.spi.FileSystemProvider

/** Abstract base for paths on a compressed filesystem.
 *
 * Compressed paths are of the format
 *
 * <pre>
 *   xyz:/path/to/file.xyz
 *   xyz:file:///path/to/file.xyz
 *   xyz:http://server/path/to/file.xyz
 *
 * </pre>
 *
 * Compressed streams are always service from another filesystem.
 *
 * @since 0.1
 */
@CompileStatic
abstract class AbstractCompressedPath extends AbstractPath {

    /**
     * Returns the file system that created this object.
     *
     * @return the file system that created this object
     */
    @Override
    FileSystem getFileSystem() {
        this.fileSystem
    }

    /** Tells whether or not the servicing path is absolute.
     *
     * @return {@code true} if, and only if, the servicing path is absolute
     */
    @Override
    boolean isAbsolute() {
        servicePath.absolute
    }

    /** Compressed path do not have the concept of a parent.
     *
     * @return {@code null}
     */
    @Override
    Path getParent() {
        return null
    }

    /**
     * Returns a {@code Path} object representing the absolute path of this
     * path.
     *
     * <p> If this path is already {@link Path#isAbsolute absolute} then this
     * method simply returns this path. Otherwise, this method resolves the path
     * in an implementation dependent manner, typically by resolving the path
     * against a file system default directory. Depending on the implementation,
     * this method may throw an I/O error if the file system is not accessible.
     *
     * @return a {@code Path} object representing the absolute path
     *
     * @throws java.io.IOError
     *          if an I/O error occurs
     * @throws SecurityException
     *          In the case of the servicing path being on the default provider, a security manager
     *          is installed, and this path is not absolute, then the security
     *          manager's {@link SecurityManager#checkPropertyAccess(String)
     *          checkPropertyAccess} method is invoked to check access to the
     *          system property {@code user.dir}
     */
    @Override
    Path toAbsolutePath() {
        absolute ? this : createPath(servicePath.toAbsolutePath())
    }

    /** Conversions to {@link java.io.File} is not supported.
     *
     * Will always throw exception.
     *
     * @throws UnsupportedOperationException
     */
    @Override
    File toFile() {
        throw new UnsupportedOperationException("Filesystems of type '${getScheme()}' do not support conversions to java.io.File")
    }

    /**
     * Returns a URI to represent this path.
     *
     * <p> This method constructs an absolute {@link URI} with a {@link
     * URI # getScheme ( ) scheme} equal to the URI scheme that identifies the
     * provider. The exact form of the scheme specific part is highly dependent on
     * the servicing providers
     *
     * @return the URI representing this path
     *
     * @throws java.io.IOError
     *          if an I/O error occurs obtaining the absolute path, or where a
     *          file system is constructed to access the contents of a file as
     *          a file system, and the URI of the enclosing file system cannot be
     *          obtained
     *
     * @throws SecurityException
     *          In the case of the service path being provided by the default provider, and a security manager
     *          is installed, the {@link #toAbsolutePath toAbsolutePath} method
     *          throws a security exception.
     */
    @Override
    URI toUri() {
        new URI("${fileSystem.provider().getScheme()}:${servicePath.toUri()}")
    }

    /** Compressed paths do not have the concept of a root.
     *
     * @return {@code null}
     */
    @Override
    Path getRoot() {
        null
    }

    /**
     * Returns the <em>real</em> path of an existing file.
     *
     * <p> The precise definition of this method is dependent on the type of
     * filesystem that actually services the file.
     * In general it derives from this path, an {@link #isAbsolute absolute}
     * path that locates the {@link Files#isSameFile same} file as this path, but
     * with name elements that represent the actual name of the directories
     * and the file. For example, where filename comparisons on a file system
     * are case insensitive then the name elements represent the names in their
     * actual case. Additionally, the resulting path has redundant name
     * elements removed.
     *
     * <p> If this path is relative then its absolute path is first obtained,
     * as if by invoking the {@link #toAbsolutePath toAbsolutePath} method.
     *
     * <p> The {@code options} array may be used to indicate how symbolic links
     * are handled. By default, symbolic links are resolved to their final
     * target. If the option {@link java.nio.file.LinkOption#NOFOLLOW_LINKS NOFOLLOW_LINKS} is
     * present then this method does not resolve symbolic links.
     *
     * Some servicing filesystem implementations allow special names such as "{@code ..}" to refer to
     * the parent directory. When deriving the <em>real path</em>, and a
     * "{@code ..}" (or equivalent) is preceded by a non-"{@code ..}" name then
     * an implementation will typically cause both names to be removed. When
     * not resolving symbolic links and the preceding name is a symbolic link
     * then the names are only removed if it guaranteed that the resulting path
     * will locate the same file as this path.
     *
     * @param options
     *          options indicating how symbolic links are handled
     *
     * @return an absolute path represent the <em>real</em> path of the file
     *          located by this object
     *
     * @throws IOException
     *          if the file does not exist or an I/O error occurs
     * @throws SecurityException
     *          In the case of the default provider, and a security manager
     *          is installed, its {@link SecurityManager#checkRead(String) checkRead}
     *          method is invoked to check read access to the file, and where
     *          this path is not absolute, its {@link SecurityManager#checkPropertyAccess(String)
     *          checkPropertyAccess} method is invoked to check access to the
     *          system property {@code user.dir}
     */
    @Override
    Path toRealPath(LinkOption... options) throws IOException {
        createPath(servicePath.toRealPath(options))
    }

    /**
     * Returns a path that is this path with redundant name elements eliminated.
     *
     * <p> The precise definition of this method is implementation dependent but
     * in general it derives from this path, a path that does not contain
     * <em>redundant</em> name elements. In many file systems, the "{@code .}"
     * and "{@code ..}" are special names used to indicate the current directory
     * and parent directory. In such file systems all occurrences of "{@code .}"
     * are considered redundant. If a "{@code ..}" is preceded by a
     * non-"{@code ..}" name then both names are considered redundant (the
     * process to identify such names is repeated until it is no longer
     * applicable).
     *
     * <p> This method does not access the file system; the path may not locate
     * a file that exists. Eliminating "{@code ..}" and a preceding name from a
     * path may result in the path that locates a different file than the original
     * path. This can arise when the preceding name is a symbolic link.
     *
     * @return the resulting path or this path if it does not contain
     *          redundant name elements; an empty path is returned if this path
     *          does have a root component and all name elements are redundant
     *
     * @see #getParent
     * @see #toRealPath
     */
    @Override
    Path normalize() {
        createPath(servicePath.normalize())
    }

    /** For a compressed filesystem, there are not elements
     *
     * @return Always {@code 0}.
     */
    @Override
    int getNameCount() {
        return 0
    }

    /** There are no elements in a compressed filesystem.
     *
     * Always throws an exception.
     *
     * @throws IllegalArgumentException
     */
    @Override
    Path getName(int index) {
        throw new IllegalArgumentException("Filesystems of type '${getScheme()}' do not support path elements.")
    }

    /** There are no elements in a compressed filesystem.
     *
     * Always throws an exception.
     *
     * @throws IllegalArgumentException
     */
    @Override
    Path subpath(int beginIndex, int endIndex) {
        throw new IllegalArgumentException("Filesystems of type '${getScheme()}' do not support path elements.")
    }

    /** Returns an iterator over the name elements of this path.
     *
     * As compressed filesystems have no elements the iterator will always be empty.
     *
     * @return an iterator over an empty list.
     */
    @Override
    Iterator<Path> iterator() {
        ([] as List<Path>).iterator()
    }

    /**
     * Tests if this path starts with the given path.
     *
     * <p> This path <em>starts</em> with the given path if this path's root
     * component <em>starts</em> with the root component of the given path,
     * and this path starts with the same name elements as the given path.
     * If the given path has more name elements than this path then {@code false}
     * is returned.
     *
     * <p> Whether or not the root component of this path starts with the root
     * component of the given path is file system specific. If this path does
     * not have a root component and the given path has a root component then
     * this path does not start with the given path.
     *
     * <p> If the given path is associated with a different {@code FileSystem}
     * to this path then {@code false} is returned.
     *
     * @param other
     *          the given path
     *
     * @return {@code true} if this path starts with the given path; otherwise
     * {@code false}
     */
    @Override
    boolean startsWith(Path other) {

        FileSystemProvider otherProvider = other.fileSystem.provider()
        FileSystemProvider thisProvider = fileSystem.provider()

        if(otherProvider.getScheme() != thisProvider.getScheme()) {
            return false
        }

        if(other.fileSystem != this.fileSystem) {
            return false
        }

        getServiceLayerPath().startsWith(((AbstractCompressedPath)other).getServiceLayerPath())
    }

    /**
     * Tests if this path starts with a {@code Path}, constructed by converting
     * the given path string, in exactly the manner specified by the {@link
     * # startsWith ( Path ) startsWith(Path)} method.
     *
     * @param other
     *          the given path string
     *
     * @return {@code true} if this path starts with the given path; otherwise
     * {@code false}
     *
     * @throws InvalidPathException
     *          If the path string cannot be converted to a Path.
     */
    @Override
    boolean startsWith(String other) {
        startsWith(Paths.get(other))
    }

    /**
     * Tests if this path ends with the given path.
     *
     * <p> If the given path has <em>N</em> elements, and no root component,
     * and this path has <em>N</em> or more elements, then this path ends with
     * the given path if the last <em>N</em> elements of each path, starting at
     * the element farthest from the root, are equal.
     *
     * <p> If the given path has a root component then this path ends with the
     * given path if the root component of this path <em>ends with</em> the root
     * component of the given path, and the corresponding elements of both paths
     * are equal. Whether or not the root component of this path ends with the
     * root component of the given path is file system specific. If this path
     * does not have a root component and the given path has a root component
     * then this path does not end with the given path.
     *
     * <p> If the given path is associated with a different {@code FileSystem}
     * to this path then {@code false} is returned.
     *
     * @param other
     *          the given path
     *
     * @return {@code true} if this path ends with the given path; otherwise
     * {@code false}
     */
    @Override
    boolean endsWith(Path other) {
        FileSystemProvider otherProvider = other.fileSystem.provider()
        FileSystemProvider thisProvider = fileSystem.provider()

        if(otherProvider.getScheme() != thisProvider.getScheme()) {
            return false
        }

        if(other.fileSystem != this.fileSystem) {
            return false
        }

        getServiceLayerPath().endsWith(((AbstractCompressedPath)other).getServiceLayerPath())
    }

    /**
     * Tests if this path ends with a {@code Path}, constructed by converting
     * the given path string, in exactly the manner specified by the {@link
     * # endsWith ( Path ) endsWith(Path)} method. On UNIX for example, the path
     * "{@code foo/bar}" ends with "{@code foo/bar}" and "{@code bar}". It does
     * not end with "{@code r}" or "{@code /bar}". Note that trailing separators
     * are not taken into account, and so invoking this method on the {@code
     * Path}"{@code foo/bar}" with the {@code String} "{@code bar/}" returns
     * {@code true}.
     *
     * @param other
     *          the given path string
     *
     * @return {@code true} if this path ends with the given path; otherwise
     * {@code false}
     *
     * @throws InvalidPathException
     *          If the path string cannot be converted to a Path.
     */
    @Override
    boolean endsWith(String other) {
        endsWith(Paths.get(other))
    }

    /** Not supported for compressed paths.
     *
     * @throws IllegalArgumentException
     */
    @Override
    Path resolve(Path other) {
        throw new IllegalArgumentException("Filesystems of type '${getScheme()}' cannot be resolved relative to each other.")
    }

    /** Not supported for compressed paths.
     *
     * @throws IllegalArgumentException
     */
    @Override
    Path resolve(String other) {
        throw new IllegalArgumentException("Filesystems of type '${getScheme()}' cannot be resolved relative to each other.")
    }

    /** Not supported for compressed paths.
     *
     * @throws IllegalArgumentException
     */
    @Override
    Path resolveSibling(Path other) {
        throw new IllegalArgumentException("Filesystems of type '${getScheme()}' cannot be resolved relative to each other.")
    }

    /** Not supported for compressed paths.
     *
     * @throws IllegalArgumentException
     */
    @Override
    Path resolveSibling(String other) {
        throw new IllegalArgumentException("Filesystems of type '${getScheme()}' cannot be resolved relative to each other.")
    }

    /** Not supported for compressed paths.
     *
     * @throws IllegalArgumentException
     */
    @Override
    Path relativize(Path other) {
        throw new IllegalArgumentException("Filesystems of type '${getScheme()}' cannot be resolved relative to each other.")
    }

    /**
     * Compares two abstract paths lexicographically. The ordering defined by
     * this method is provider specific, and in the case of the default
     * provider, platform specific. This method does not access the file system
     * and neither file is required to exist.
     *
     * <p> This method may not be used to compare paths that are associated
     * with different file system providers.
     *
     * @param other the path compared to this path.
     *
     * @return zero if the argument is {@link #equals equal} to this path, a
     *          value less than zero if this path is lexicographically less than
     *          the argument, or a value greater than zero if this path is
     *          lexicographically greater than the argument
     *
     * @throws ClassCastException
     *          if the paths are associated with different providers or the service paths are from different providers.
     */
    @Override
    int compareTo(Path other) {
        if(fileSystem.provider() != other.fileSystem.provider()) {
            throw new ClassCastException("Cannot compare paths from '${getScheme()}' scheme with paths from '${other.fileSystem.provider().getScheme()}' scheme.")
        }


        return getServiceLayerPath().compareTo( ((AbstractCompressedPath)other).getServiceLayerPath() )
    }

    /** In this implementation events are not supported for compressed filesystem.
     *
     * <p> Always throws exception.
     *
     * @throws UnsupportedOperationException
     *
     */
    @Override
    WatchKey register(WatchService watcher, WatchEvent.Kind<?>[] events, WatchEvent.Modifier... modifiers) throws IOException {
        throw new UnsupportedOperationException("Filesystem scheme '${getScheme()}' do not support events")
    }

    /** In this implementation events are not supported for compressed filesystem.
     *
     * <p> Always throws exception.
     *
     * @throws UnsupportedOperationException
     *
     */
    @Override
    WatchKey register(WatchService watcher, WatchEvent.Kind<?>[] events) throws IOException {
        throw new UnsupportedOperationException("Filesystem scheme '${getScheme()}' do not support events")
    }

    /** There are no elements in a compressed filesystem.
     *
     * @return Always {@code null}.
     */
    @Override
    Path getFileName() {
        null
    }

    /** Access to the next layer which actually serviced the compressed content.
     *
     * @return The current path that services the compressed content.
     *
     * @since 0.1
     */
    Path getServiceLayerPath() {
        this.servicePath
    }

    /** Represents the compressed path + the secondary path.
     *
     * @return a string representation of the object.
     */
    @Override
    String toString() {
        "${getScheme()}:${servicePath.toString()}"
    }

    /** Constructs some form of compressed path.
     *
     * @param fs Filesystem this path is tied to.
     * @param fileServiceLayer Path that will actually service the file
     *
     * @since 0.1
     */
    protected AbstractCompressedPath(AbstractCompressedFileSystem fs, Path fileServiceLayer, Class pathClass ) {
        this.pathClass = pathClass
        this.fileSystem = fs
        this.servicePath = fileServiceLayer
    }

    /** Creates a new path from this filesystem
     *
     * @param servicingPath
     * @return
     *
     * @since 0.1
     */
    protected Path createPath(Path servicingPath) {
        (Path)(pathClass.newInstance(getFileSystem(),servicePath))
    }

    /** Compress paths do not have path components and therefore no separator.
     *
     * @return {@code null}
     */
    @Override
    protected String getSeparator() {
        null
    }

    /** Compress paths do not have the concept of a current directory.
     *
     * @return {@code null}
     */
    @Override
    protected String getCurrentDirAlias() {
        null
    }

    /** Compress paths do not have the concept of a parent directory.
     *
     * @return {@code null}
     */
    @Override
    protected String getParentDirAlias() {
        null
    }

    /** Compressed filesystems do not have the concept of a root.
     *
     * @return {@code null}
     */
    @Override
    protected Path getResolvableRoot() {
        null
    }

    /** Gets the scheme associated with this filesystem.
     *
     * <p> This effectively calls {@code getScheme()} on the provider.
     *
     * @return Scheme name
     *
     * @since 0.1
     */
    protected String getScheme() {
        fileSystem.provider().getScheme()
    }

    private Class pathClass
    private AbstractCompressedFileSystem fileSystem
    private Path servicePath

}
