/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.core

import groovy.transform.CompileStatic

import java.nio.file.OpenOption

/** Provides a generic of presenting various options used by provider implementation in a text free-form
 * approach.
 *
 * <p>
 *     This is useful when options are specified in a fully-qualified property name convention.
 *
 * @since 0.1
 */
@CompileStatic
class ProviderOpts {

    /** Adds a set of {@link java.nio.file.OpenOption} values.
     *
     * <p> This can replace a call to the variable arguments list in a call
     *   like {@link java.nio.file.Files#newInputStream}.
     *
     * <pre>
     *     Files.newInputStream( path, ProviderOpts.open(
     *        'org.ysb33r.nio.provider.gz.compression : '9',
     *        StandardOpenOption.WRITE
     *     )
     * </pre>
     *
     * @param opts1 Free form options.
     * @param opts2 Any other options that implemenent {@link java.nio.file.OpenOption}
     * @return An array of options suitable for passing to something like {@link java.nio.file.Files#newInputStream}.
     *
     * @since 0.1
     */
    static OpenOption[] open(final Map<String, ?> opts1, OpenOption... opts2) {
        ProviderOpts.open(opts1, opts2 as List)
    }

    /** Adds a set of {@link java.nio.file.OpenOption} values.
     *
     * <p> This can replace a call to the variable arguments list in a call
     *   like {@link java.nio.file.Files#newInputStream}.
     *
     * <pre>
     *     Files.newInputStream( path, ProviderOpts.open(
     *        'org.ysb33r.nio.provider.gz.compression : '9',
     *        StandardOpenOption.WRITE
     *     )
     * </pre>
     *
     * @param opts1 Free form options.
     * @param opts2 Any other options that implemennt {@link java.nio.file.OpenOption}
     * @return An array of options suitable for passing to something like {@link java.nio.file.Files#newInputStream}.
     *
     * @since 0.1
     */
    static OpenOption[] open(Map<String, ?> opts1, Iterable<OpenOption> opts2) {

        List<OpenOption> newOpts = opts1.collect { String k, Object v ->
            (OpenOption) (new ProviderOpts.FreeFormOpenOption(k, v))
        }
        newOpts.addAll opts2

        newOpts.toArray(new OpenOption[newOpts.size()])
    }

    /* A generic free form option presentation.
     *
     * @since 0.1
     */
    static class FreeFormOpenOption extends AbstractMap.SimpleEntry<String,Object> implements OpenOption {

        FreeFormOpenOption(final String name, final Object value) {
            super(name, value)
        }
    }
}
