/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.core.compressed

import groovy.transform.CompileStatic

import java.nio.file.FileSystems
import java.nio.file.ProviderNotFoundException
import java.nio.file.spi.FileSystemProvider

/** Extracts a secondary filesystem from a URI.
 *
 * @since 0.1
 */
@CompileStatic
class FileSystemLayerParser {

    /** Takes a URI and extracts the provider for secondary filesystem
     *
     * @param uri URI containing more than one scheme layer.
     * @return Secondary filesystem.
     *
     * @throw java.nio.file.ProviderNotFoundException if secondary filesystem provider has not yet been loaded.
     *
     * @since 0.1
     */
    static SecondaryFileSystemInfo extractSecondaryFileSystem(final URI uri) {
        FileSystemProvider provider
        URI extractedUri
        URI rootUri
        if(uri.opaque) {
            extractedUri = uri.rawSchemeSpecificPart.toURI()

            if(extractedUri.scheme) {
                provider = FileSystemProvider.installedProviders().find { FileSystemProvider p ->
                    p.getScheme() == extractedUri.scheme
                }

                if(provider==null) {
                    throw new ProviderNotFoundException("${uri} requires provider for '${extractedUri.scheme}', but not provider could be found.")
                }

                if(extractedUri.authority) {
                    rootUri = new URI(extractedUri.scheme,extractedUri.authority,'/',null,null)
                } else {
                    rootUri = new URI(extractedUri.scheme,null,'/',null,null)
                }
            } else {
                provider = FileSystems.default.provider()
                extractedUri = new File(extractedUri.path).absoluteFile.toURI()
                rootUri = 'file:///'.toURI()
            }

        } else {
            provider = FileSystems.default.provider()
            extractedUri = "file://${uri.path}".toURI()
            rootUri = 'file:///'.toURI()
        }

        [
            getSecondaryProvider : { ->
                provider
            },

            getSecondaryURI : { ->
                extractedUri
            },

            getSecondaryRootURI : { ->
                rootUri
            }

        ] as SecondaryFileSystemInfo
    }
}
