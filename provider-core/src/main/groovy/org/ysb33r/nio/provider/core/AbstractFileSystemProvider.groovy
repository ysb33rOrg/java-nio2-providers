/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.core

import groovy.transform.CompileStatic

import java.nio.channels.SeekableByteChannel
import java.nio.file.OpenOption
import java.nio.file.Path
import java.nio.file.attribute.FileAttribute
import java.nio.file.spi.FileSystemProvider

/** Implements boilerplate methods for a {@link FileSystemProvider}
 *
 * @since 0.1
 */
@CompileStatic
abstract class AbstractFileSystemProvider extends FileSystemProvider {

    AbstractFileSystemProvider(final String scheme) {
        super()
        this.scheme = System.getProperty(getSchemeSystemPropertyName(scheme)) ?: scheme
    }

    /** Returns the scheme that this filesystem provider was constructed with.
     *
     * @return Name of scheme.
     */
    @Override
    String getScheme() {
        this.scheme
    }

    /**
     * Tests if two paths locate the same file. This method works in exactly the
     * manner specified by the {@link java.nio.file.Files#isSameFile} method.
     * In addition it will check if two paths are on the same filesystem, before
     * checking whether they locate the same file.
     *
     * @param path
     *          one path to the file
     * @param path2
     *          the other path
     *
     * @return {@code true} if, and only if, the two paths locate the same file
     *
     * @throws IOException
     *          if an I/O error occurs
     * @throws SecurityException
     *          In the case of the default provider, and a security manager is
     *          installed, the {@link SecurityManager#checkRead(String) checkRead}
     *          method is invoked to check read access to both files.
     */
    @Override
    // tag::isSameFile[]
    boolean isSameFile(Path path, Path path2) throws IOException {

        AbstractPath p1 = (AbstractPath) path  // <1>
        AbstractPath p2 = (AbstractPath) path2

        if (p1.fileSystem != p2.fileSystem) {
            return false
        }

        if (!hasReadAccess(p1)) {
            throw new SecurityException("No read access to '${path}'")
        }

        if (!hasReadAccess(p2)) {
            throw new SecurityException("No read access to '${path2}'")
        }

        doCheckIfSameFile(p1, p2)
    }
    // end::isSameFile[]

    /** Returns the section out of a URI that will represent a path on this filesystem
     *
     * @param uri
     * @return
     */
    protected String extractFilePath(final URI uri) {
        uri.path
    }

    /** Validates the URI whether it is valid within the context of this provider
     *
     * @param uri
     * @throw {@code IllegalArgumentException} if URI is not valid.
     */
    protected void validateURI(final URI uri) {
        if (uri.isOpaque()) {
            throw new IllegalArgumentException("Opaque URIs (${uri.scheme} not followed by //) are not allowed in this context.")
        }
        if (uri.scheme != scheme) {
            throw new IllegalArgumentException("Provided scheme '${uri.scheme}' is invalid in this context. Expected '${scheme}'.")
        }
    }

    /** Returns a property name which can be used to override the default scheme name.
     *
     * @param scheme Default name of scheme
     * @return System property name
     */
    protected String getSchemeSystemPropertyName(final String scheme) {
        "org.ysb33r.nio.provider.${scheme}.scheme"
    }

    /** Extract the view name from an attribute when it is in the form of {@code [view:attr]}.
     *
     * @param attribute Name of attribute
     * @return View name if found, otherwise {@code null}.
     */
    protected String getViewNameFromAttribute(final String attribute) {
        String[] parts = attribute.split(':')
        parts.size() > 1 ? parts[0] : null
    }

    abstract protected boolean hasReadAccess(AbstractPath path)

    abstract protected boolean doCheckIfSameFile(AbstractPath path1, AbstractPath path2)

    private final String scheme
}
