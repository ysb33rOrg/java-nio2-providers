/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.core.compressed

import groovy.transform.CompileStatic
import org.ysb33r.nio.provider.core.registry.FileSystemRegistry

import java.nio.file.FileSystem
import java.nio.file.FileSystemAlreadyExistsException
import java.nio.file.FileSystemNotFoundException
import java.util.concurrent.ConcurrentHashMap

/** A special registry for keeping compressed filesystems.
 *
 * @since 0.1
 */
@CompileStatic
class CompressedFileSystemRegistry implements FileSystemRegistry<FileSystem> {

    /** See if a filesystem related to the specific key exists.
     *
     * @param key Filesystem key
     * @return {@code true} is filesystem exists for the given key.
     *
     * @since 0.1
     */
    @Override
    boolean contains(FileSystem filesystem) {
        registry.containsKey(filesystem)
    }

    /** Adds a filesystems if it does not exist.
     *
     * @param key Key to use to store filesystem
     * @param fs {@code FileSystem} instance.
     * @throw {@code FileSystemAlreadyExistsException} if a filesystem exists for the given key.
     *
     * @since 0.1
     */
    @Override
    void add(FileSystem key, FileSystem fs) {
        if(null != registry.putIfAbsent(key,fs)) {
            throw new FileSystemAlreadyExistsException("Filesystem for key '${key.toString()}' already exists")
        }
    }

    /** Adds a filesystems if it does not exist.
     *
     * @param key Key to use to store filesystem
     * @param fs {@code FileSystem} instance.
     * @throw {@code FileSystemAlreadyExistsException} if a filesystem exists for the given key.
     *
     * @since 0.1
     */
    void replace(FileSystem key, FileSystem fs) {
        if(registry.containsKey(key)) {
            registry.put(key,fs)
        } else {
            throw new FileSystemNotFoundException("Filesystem for key '${key.toString()}' does not exist")
        }
    }

    /** Returns the filesystem that is associated with the specific key.
     *
     * @param key Key to use to retrieve filesystem
     * @return {@code FileSystem} instance.
     * @throw {@code FileSystemNotFoundException} is no filesystem is associated with the given key.
     *
     * @since 0.1
     */
    @Override
    FileSystem get(FileSystem filesystem) {
        FileSystem fs = registry.get(filesystem)

        if(fs == null) {
            throw new FileSystemNotFoundException("No filesystem is associated with key '${filesystem.toString()}'")
        }

        return fs
    }

    private final ConcurrentHashMap<FileSystem,FileSystem> registry = new ConcurrentHashMap<FileSystem,FileSystem>()

}
