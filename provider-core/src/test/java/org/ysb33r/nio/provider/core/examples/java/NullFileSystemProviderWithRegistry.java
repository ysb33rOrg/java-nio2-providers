/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.core.examples.java;

import org.ysb33r.nio.provider.core.AbstractFileSystemProviderWithRegistry;
import org.ysb33r.nio.provider.core.AbstractPath;
import org.ysb33r.nio.provider.core.registry.FileSystemRegistry;
import org.ysb33r.nio.provider.core.registry.SimpleFileSystemRegistry;

import java.io.IOException;
import java.net.URI;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.FileAttributeView;
import java.util.Map;
import java.util.Set;

// tag::nullfsp[]
public class NullFileSystemProviderWithRegistry extends AbstractFileSystemProviderWithRegistry {

    NullFileSystemProviderWithRegistry() {
        super( "null", new SimpleFileSystemRegistry() ); // <1>
    }
    // end::nullfsp[]

    @Override
    protected FileSystem doCreateFileSystem(URI uri, Map<String, ?> env) {
        return null;
    }

    @Override
    protected boolean hasReadAccess(AbstractPath path) {
        return false;
    }

    @Override
    protected boolean doCheckIfSameFile(AbstractPath path1, AbstractPath path2) {
        return false;
    }

    @Override
    public Path getPath(URI uri) {
        return null;
    }

    @Override
    public SeekableByteChannel newByteChannel(Path path, Set<? extends OpenOption> options, FileAttribute<?>... attrs) throws IOException {
        return null;
    }

    @Override
    public DirectoryStream<Path> newDirectoryStream(Path dir, DirectoryStream.Filter<? super Path> filter) throws IOException {
        return null;
    }

    @Override
    public void createDirectory(Path dir, FileAttribute<?>... attrs) throws IOException {

    }

    @Override
    public void delete(Path path) throws IOException {

    }

    @Override
    public void copy(Path source, Path target, CopyOption... options) throws IOException {

    }

    @Override
    public void move(Path source, Path target, CopyOption... options) throws IOException {

    }

    @Override
    public boolean isHidden(Path path) throws IOException {
        return false;
    }

    @Override
    public FileStore getFileStore(Path path) throws IOException {
        return null;
    }

    @Override
    public void checkAccess(Path path, AccessMode... modes) throws IOException {

    }

    @Override
    public <V extends FileAttributeView> V getFileAttributeView(Path path, Class<V> type, LinkOption... options) {
        return null;
    }

    @Override
    public <A extends BasicFileAttributes> A readAttributes(Path path, Class<A> type, LinkOption... options) throws IOException {
        return null;
    }

    @Override
    public Map<String, Object> readAttributes(Path path, String attributes, LinkOption... options) throws IOException {
        return null;
    }

    @Override
    public void setAttribute(Path path, String attribute, Object value, LinkOption... options) throws IOException {

    }

    @Override
    public FileSystemRegistry getFileSystemRegistry() {
        return null;
    }

// tag::nullfsp[]
}
// end::nullfsp[]

