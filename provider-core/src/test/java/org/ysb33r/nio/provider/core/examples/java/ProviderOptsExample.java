/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.core.examples.java;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

// tag::static-imports[]
import static java.nio.file.StandardOpenOption.CREATE_NEW;
import static java.nio.file.StandardOpenOption.WRITE;
import static org.ysb33r.nio.provider.core.ProviderOpts.open;
// end::static-imports[]

public class ProviderOptsExample {
    void example() {
        try {
            // tag::example[]
            Map<String, Object> options = new HashMap<String, Object>() { // <1>
                {
                    put("org.ysb33r.nio.provider.gz.compression", 9);
                }
            };
            BufferedWriter writer = Files.newBufferedWriter(
                    Paths.get("gz:myfile.gz"),
                    open(options, WRITE, CREATE_NEW) // <2>
            );
            // end::example[]
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
