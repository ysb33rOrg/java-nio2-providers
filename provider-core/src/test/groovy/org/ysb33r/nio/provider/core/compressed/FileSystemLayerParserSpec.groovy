/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.core.compressed

import spock.lang.Specification

import java.nio.file.FileSystems

class FileSystemLayerParserSpec extends Specification {

    def 'When second layer is absolute path then it should be converted to an absolute file:// URI'() {

        given: 'A URI such as foo:/path/file.gz which assumes local filesystem'
        URI uri = 'foo:/path/file.gz'.toURI()

        when: 'The URI is parsed'
        SecondaryFileSystemInfo info = FileSystemLayerParser.extractSecondaryFileSystem(uri)

        then: 'The provider is the default provider'
        info.secondaryProvider == FileSystems.getDefault().provider()

        and: 'The URI will be an absolute URI pointing to the local filesystem'
        info.secondaryURI == new File('/path/file.gz').absoluteFile.toURI()
        info.secondaryURI.path.startsWith('/')
        info.secondaryRootURI == 'file:///'.toURI()
    }

    def 'When second layer is a URI then it should be used asis'() {

        given: 'A URI such as foo:file:///path/file.gz which assumes local filesystem'
        URI uri = 'foo:file:///path/file.gz'.toURI()

        when: 'The URI is parsed'
        SecondaryFileSystemInfo info = FileSystemLayerParser.extractSecondaryFileSystem(uri)

        then: 'The provider is the default provider'
        info.secondaryProvider == FileSystems.getDefault().provider()

        and: 'The URI will be an absolute URI pointing to the local filesystem'
        info.secondaryURI == 'file:///path/file.gz'.toURI()
        info.secondaryURI.path.startsWith('/')
        info.secondaryRootURI == 'file:///'.toURI()
    }

    def 'When second layer is relative path then it should be converted to an absolute file:// URI'() {

        given: 'A URI such as foo:file.gz which assumes local filesystem'
        URI uri = 'foo:file.gz'.toURI()

        when: 'The URI is parsed'
        SecondaryFileSystemInfo info = FileSystemLayerParser.extractSecondaryFileSystem(uri)

        then: 'The provider is the default provider'
        info.secondaryProvider == FileSystems.getDefault().provider()

        and: 'The URI will be an absolute URI pointing to the local filesystem'
        info.secondaryURI == new File('file.gz').absoluteFile.toURI()
        info.secondaryURI.path.startsWith('/')
        info.secondaryRootURI == 'file:///'.toURI()
    }
}