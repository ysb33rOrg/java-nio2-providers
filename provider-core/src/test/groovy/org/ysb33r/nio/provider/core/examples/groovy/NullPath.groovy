/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.core.examples.groovy

import groovy.transform.CompileStatic
import org.ysb33r.nio.provider.core.AbstractPosixPath

import java.nio.file.LinkOption
import java.nio.file.Path
import java.nio.file.WatchEvent
import java.nio.file.WatchKey
import java.nio.file.WatchService


@CompileStatic
class NullPath extends AbstractPosixPath<NullFileSystem> {

    NullPath(NullFileSystem fs, String authority, String path, String... more) {
        super(fs, authority, path, more)
    }

    NullPath(NullFileSystem fs, String authority) {
        super(fs, authority)
    }

    @Override
    protected Path createPath(String part, String... more) {
        return null
    }

    @Override
    protected Path createPath() {
        return null
    }

    @Override
    Path toRealPath(LinkOption... options) throws IOException {
        return null
    }

    @Override
    WatchKey register(WatchService watcher, WatchEvent.Kind<?>[] events, WatchEvent.Modifier... modifiers) throws IOException {
        return null
    }

    @Override
    WatchKey register(WatchService watcher, WatchEvent.Kind<?>... events) throws IOException {
        return null
    }
}
