/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.core.examples.groovy

import groovy.transform.CompileStatic
import org.ysb33r.nio.provider.core.AbstractFileSystemProviderWithRegistry
import org.ysb33r.nio.provider.core.AbstractPath
import org.ysb33r.nio.provider.core.registry.FileSystemRegistry
import org.ysb33r.nio.provider.core.registry.SimpleFileSystemRegistry

import java.nio.channels.SeekableByteChannel
import java.nio.file.AccessMode
import java.nio.file.CopyOption
import java.nio.file.DirectoryStream
import java.nio.file.FileStore
import java.nio.file.FileSystem
import java.nio.file.LinkOption
import java.nio.file.OpenOption
import java.nio.file.Path
import java.nio.file.attribute.BasicFileAttributes
import java.nio.file.attribute.FileAttribute
import java.nio.file.attribute.FileAttributeView

// tag::nullfsp[]
@CompileStatic
class NullFileSystemProviderWithRegistry extends AbstractFileSystemProviderWithRegistry {

    NullFileSystemProviderWithRegistry(String scheme, FileSystemRegistry registry) {
        super('null', new SimpleFileSystemRegistry() ) // <1>
    }
    // end::nullfsp[]

    @Override
    protected FileSystem doCreateFileSystem(URI uri, Map<String, ?> env) {
        return null
    }

    // -----
    @Override
    protected boolean hasReadAccess(AbstractPath path) {
        return false
    }

    @Override
    protected boolean doCheckIfSameFile(AbstractPath path1, AbstractPath path2) {
        return false
    }

    // -----

    @Override
    FileSystemRegistry getFileSystemRegistry() {
        return null
    }
    // -----

    @Override
    Path getPath(URI uri) {
        return null
    }

    @Override
    SeekableByteChannel newByteChannel(Path path, Set<? extends OpenOption> options, FileAttribute<?>... attrs) throws IOException {
        return null
    }

    @Override
    DirectoryStream<Path> newDirectoryStream(Path dir, DirectoryStream.Filter<? super Path> filter) throws IOException {
        return null
    }

    @Override
    void createDirectory(Path dir, FileAttribute<?>... attrs) throws IOException {

    }

    @Override
    void delete(Path path) throws IOException {

    }

    @Override
    void copy(Path source, Path target, CopyOption... options) throws IOException {

    }

    @Override
    void move(Path source, Path target, CopyOption... options) throws IOException {

    }

    @Override
    boolean isHidden(Path path) throws IOException {
        return false
    }

    @Override
    FileStore getFileStore(Path path) throws IOException {
        return null
    }

    @Override
    void checkAccess(Path path, AccessMode... modes) throws IOException {

    }

    @Override
    def <V extends FileAttributeView> V getFileAttributeView(Path path, Class<V> type, LinkOption... options) {
        return null
    }

    @Override
    def <A extends BasicFileAttributes> A readAttributes(Path path, Class<A> type, LinkOption... options) throws IOException {
        return null
    }

    @Override
    Map<String, Object> readAttributes(Path path, String attributes, LinkOption... options) throws IOException {
        return null
    }

    @Override
    void setAttribute(Path path, String attribute, Object value, LinkOption... options) throws IOException {

    }

// tag::nullfsp[]
}
// end::nullfsp[]
