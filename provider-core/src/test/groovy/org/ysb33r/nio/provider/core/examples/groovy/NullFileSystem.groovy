/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.core.examples.groovy

import groovy.transform.CompileStatic
import org.ysb33r.nio.provider.core.AbstractFileSystem

import java.nio.file.FileStore
import java.nio.file.Path
import java.nio.file.PathMatcher
import java.nio.file.WatchService
import java.nio.file.attribute.UserPrincipalLookupService
import java.nio.file.spi.FileSystemProvider

/**
 * @since
 */
// tag::nullfsp[]
@CompileStatic
class NullFileSystem extends AbstractFileSystem {

    NullFileSystem(NullFileSystemProvider provider) {
        super(provider, null) // <1>
    }
// end::nullfsp[]

    @Override
    protected void doCloseFileSystem() {

    }

    @Override
    protected void doCloseChannels() {

    }

    @Override
    protected void doCloseDirectoryStreams() {

    }

    @Override
    protected void doCloseWatchServices() {

    }

    @Override
    boolean isReadOnly() {
        return false
    }

    @Override
    Iterable<Path> getRootDirectories() {
        return null
    }

    @Override
    Iterable<FileStore> getFileStores() {
        return null
    }

    @Override
    Set<String> supportedFileAttributeViews() {
        return null
    }

    @Override
    Path getPath(String first, String... more) {
        return null
    }

    @Override
    PathMatcher getPathMatcher(String syntaxAndPattern) {
        return null
    }

    @Override
    UserPrincipalLookupService getUserPrincipalLookupService() {
        return null
    }

    @Override
    WatchService newWatchService() throws IOException {
        return null
    }

// tag::nullfsp[]
}
// end::nullfsp[]
