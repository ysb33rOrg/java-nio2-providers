/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.core.examples.groovy

import org.ysb33r.nio.provider.core.AbstractPath
import org.ysb33r.nio.provider.core.FileStoreProvider
import org.ysb33r.nio.provider.core.compressed.AbstractCompressedFileSystem
import org.ysb33r.nio.provider.core.compressed.AbstractCompressedFileSystemProvider
import org.ysb33r.nio.provider.core.compressed.AbstractCompressedPath

import java.nio.channels.SeekableByteChannel
import java.nio.file.CopyOption
import java.nio.file.LinkOption
import java.nio.file.OpenOption
import java.nio.file.Path
import java.nio.file.attribute.FileAttribute
import java.nio.file.attribute.FileAttributeView

class GzFileSystemProvider extends AbstractCompressedFileSystemProvider {
    GzFileSystemProvider(String scheme) {
        super(scheme)
    }

    @Override
    protected AbstractCompressedFileSystem doCreateCompressedFileSystem(URI uri, FileStoreProvider fileStoreProvider, Map<String, ?> opts) {
        return null
    }

    @Override
    protected AbstractCompressedPath doCreateCompressedPath(AbstractCompressedFileSystem fs, Path servicePath) {
        return null
    }

    @Override
    protected boolean hasReadAccess(AbstractPath path) {
        return false
    }

    @Override
    protected boolean doCheckIfSameFile(AbstractPath path1, AbstractPath path2) {
        return false
    }

    @Override
    SeekableByteChannel newByteChannel(Path path, Set<? extends OpenOption> options, FileAttribute<?>... attrs) throws IOException {
        return null
    }

    @Override
    void createDirectory(Path dir, FileAttribute<?>... attrs) throws IOException {

    }

    @Override
    void delete(Path path) throws IOException {

    }

    @Override
    void copy(Path source, Path target, CopyOption... options) throws IOException {

    }

    @Override
    void move(Path source, Path target, CopyOption... options) throws IOException {

    }

    @Override
    def <V extends FileAttributeView> V getFileAttributeView(Path path, Class<V> type, LinkOption... options) {
        return null
    }

    @Override
    void setAttribute(Path path, String attribute, Object value, LinkOption... options) throws IOException {

    }
}
