/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.core.examples.groovy

import java.nio.file.Files
import java.nio.file.Paths

// tag::static-imports[]
import static org.ysb33r.nio.provider.core.ProviderOpts.open
import static java.nio.file.StandardOpenOption.CREATE_NEW
import static java.nio.file.StandardOpenOption.WRITE
// end::static-imports[]

class ProviderOptsExample {

    void example() {

        // tag::example[]
        BufferedWriter writer = Files.newBufferedWriter(
            Paths.get('gz:myfile.gz'),
            open( 'org.ysb33r.nio.provider.gz.compression' : 9, WRITE, CREATE_NEW) // <1>
        )
        // end::example[]
    }
}
