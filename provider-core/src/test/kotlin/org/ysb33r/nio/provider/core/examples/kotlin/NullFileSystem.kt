/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.core.examples.kotlin

import org.ysb33r.nio.provider.core.AbstractFileSystem
import java.nio.file.FileStore
import java.nio.file.Path
import java.nio.file.PathMatcher
import java.nio.file.WatchService
import java.nio.file.attribute.UserPrincipalLookupService
import java.nio.file.spi.FileSystemProvider


// tag::nullfsp[]
class NullFileSystem : AbstractFileSystem {
    constructor(provider: FileSystemProvider) : super(provider, null) // <1>

    // end::nullfsp[]
    override fun doCloseDirectoryStreams() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun newWatchService(): WatchService {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun supportedFileAttributeViews(): MutableSet<String> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getFileStores(): MutableIterable<FileStore> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun doCloseWatchServices() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getUserPrincipalLookupService(): UserPrincipalLookupService {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun doCloseFileSystem() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun isReadOnly(): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getPath(first: String?, vararg more: String?): Path {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun doCloseChannels() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getPathMatcher(syntaxAndPattern: String?): PathMatcher {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getRootDirectories(): MutableIterable<Path> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    // tag::nullfsp[]
}
// end::nullfsp[]
