/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.core.examples.kotlin

import org.ysb33r.nio.provider.core.ProviderOpts
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardOpenOption


class ProviderOptsExample {
    init {
        // tag::example[]
        val writer = Files.newBufferedWriter(
            Paths.get("gz:myfile.gz"),
            *ProviderOpts.open( // <1> <2>
                hashMapOf("org.ysb33r.nio.provider.gz.compression" to 9), // <3>
                StandardOpenOption.WRITE, // <4>
                StandardOpenOption.CREATE_NEW
            )
        )
        // end::example[]
    }
}