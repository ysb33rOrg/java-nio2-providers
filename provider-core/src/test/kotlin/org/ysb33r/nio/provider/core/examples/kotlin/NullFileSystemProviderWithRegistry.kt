/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2013-2018
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.nio.provider.core.examples.kotlin

import org.ysb33r.nio.provider.core.AbstractFileSystemProviderWithRegistry
import org.ysb33r.nio.provider.core.AbstractPath
import org.ysb33r.nio.provider.core.registry.FileSystemRegistry
import org.ysb33r.nio.provider.core.registry.SimpleFileSystemRegistry
import java.net.URI
import java.nio.channels.SeekableByteChannel
import java.nio.file.*
import java.nio.file.attribute.BasicFileAttributes
import java.nio.file.attribute.FileAttribute
import java.nio.file.attribute.FileAttributeView

// tag::nullfsp[]
class NullFileSystemProviderWithRegistry : AbstractFileSystemProviderWithRegistry {
    constructor(scheme: String, registry: FileSystemRegistry<*>) : super("null", SimpleFileSystemRegistry())  // <1>
    // end::nullfsp[]

    override fun checkAccess(path: Path?, vararg modes: AccessMode?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun copy(source: Path?, target: Path?, vararg options: CopyOption?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun <V : FileAttributeView?> getFileAttributeView(path: Path?, type: Class<V>?, vararg options: LinkOption?): V {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun isHidden(path: Path?): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun newDirectoryStream(dir: Path?, filter: DirectoryStream.Filter<in Path>?): DirectoryStream<Path> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hasReadAccess(path: AbstractPath?): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun newByteChannel(path: Path?, options: MutableSet<out OpenOption>?, vararg attrs: FileAttribute<*>?): SeekableByteChannel {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun delete(path: Path?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun <A : BasicFileAttributes?> readAttributes(path: Path?, type: Class<A>?, vararg options: LinkOption?): A {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun readAttributes(path: Path?, attributes: String?, vararg options: LinkOption?): MutableMap<String, Any> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun doCheckIfSameFile(path1: AbstractPath?, path2: AbstractPath?): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getFileSystemRegistry(): FileSystemRegistry<*> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getPath(uri: URI?): Path {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getFileStore(path: Path?): FileStore {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun setAttribute(path: Path?, attribute: String?, value: Any?, vararg options: LinkOption?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun move(source: Path?, target: Path?, vararg options: CopyOption?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun createDirectory(dir: Path?, vararg attrs: FileAttribute<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun doCreateFileSystem(uri: URI?, env: MutableMap<String, *>?): FileSystem {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

// tag::nullfsp[]
}
// end::nullfsp[]
